<?php

/*
* A class file to connect to database
*/
class DB_CONNECT {
	
	//constructor
	function __construct() {
		//connecting to database
		$this->connect();
	}
	
	//destructor
	function __destruct() {
		//closing db connection
		$this->close();
	}
	
	/*
	*Function to connect with database
	*/
	function connect() {
		//import database connection variables
		require_once __DIR__ . '/db_config.php';
		
		//connecting to mysql database
		$con = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die(mysql_error());
		
		//selecting database
		$db = mysql_select_db(DB_DATABASE) or die(mysql_error());
		
		mysql_select_db($db,$con);
		mysql_query("SET character_set_results=utf8");
	    mysql_query("SET character_set_client=utf8");
		mysql_query("SET character_set_connection=utf8");
		
		//returning connection cursor
		return $con;
	}
	
	function close() {
		//closing db connection
		mysql_close();
	}
}
?>
