<?php
	error_reporting(0);
	ob_start();
	date_default_timezone_set('Asia/Bangkok');
	$name =  $_SESSION["strName"];
	include("config.php");

?>

<!DOCTYPE html>
<html>
    <script src="jquery-2.1.3.min.js"></script>

    <link href="bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="bootstrap-3.3.7-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <script src="bootstrap-3.3.7-dist/js/bootstrap.js"></script>

    <link href="dist/css/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="dist/js/bootstrap-datepicker-custom.js"></script>
    <script src="dist/locales/bootstrap-datepicker.th.min.js" charset="UTF-8"></script>

    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true              //Set เป็นปี พ.ศ.
            }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
        });

    </script>
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    border: none;
    text-align: left;
    padding: 8px;
}

input[type=text], select ,textarea {
    width: 100%;
	padding: 5px 1px;
    margin: 1px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=submit] {
  
    background-color: #337ab7;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}
input[type=reset] {
   
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

</style>
<body>
 <?php

 if ($error != '')
 {
 echo '<div style="padding:4px; border:1px solid red; color:red;">'.$error.'</div>';
 }
//$assID = $_GET['assID'];
include("config.php");

//$name

$result_name = mysql_query("SELECT * FROM users WHERE username='$name'")

 or die(mysql_error());
$row_name = mysql_fetch_array($result_name);

$row_name_on = $row_name["id"];

$result = mysql_query("SELECT * FROM assets WHERE owner_user_id='$row_name_on'")

 or die(mysql_error());
$row = mysql_fetch_array($result);

 ?>


  <form action="AssetsManage" method="post">
  <div style="overflow-x:auto;">
  <table>
  <tr>
  <td width="20%"rowspan="5" style="border-style:ridge"><img src="images/<?php echo $row["img"];?>" width="75%"></td><td width="16%" bgcolor="#f9f9f9"><b>เลขครุภัณฑ์</b></td><td width="64%" bgcolor="#f9f9f9"><?php echo $row["code2"];?></td>
  </tr>
    <tr>
    <td><b>รายละเอียด</b></td><td><?php echo $row["detail"];?></td>
  </tr>
    <tr>
    <td bgcolor="#f9f9f9"><b>ประเภท/หมวดหมู่</b></td><td bgcolor="#f9f9f9">
    <?php
$cat = $row["cat_id"];
$result_c = mysql_query("SELECT * FROM categories WHERE id='$cat'")
 or die(mysql_error());
$row_c = mysql_fetch_array($result_c);
echo $row_c["description"];?>
    </td>
  </tr>
    <tr>
    <td><b>เอกสาร</b></td><td><?php echo $row["doc_ref"];?></td>
  </tr>
   <tr>
    <td bgcolor="#f9f9f9"><b>แหล่งที่มา (Supplier)</b></td><td bgcolor="#f9f9f9"><?php 
	 $result_sub = mysql_query("SELECT * FROM locations WHERE id ='".$row["supplier_name"]."'")
 or die(mysql_error());
$row_sub = mysql_fetch_array($result_sub);
	
	echo $row_sub["description"];?></td>
  </tr>
  </table>
  <table><br><br>
<tr>
<td><b>รหัส GFMIS</b></td>
<td><b>วันที่ได้มา</b></td>
<td><b>ราคาต่อหน่วย(บาท)</b></td>
<td><b>อายุการใช้งาน(ปี)</b></td>
<td><b>แหล่งงบ</b></td>
<td><b>ปีงบประมาณ</b></td>
<td><b>วิธีการได้มา</b></td>
<td><b>ใช้ประจำที่</b></td>
<td><b>สถานะ</b></td>
</tr>
<tr>
<td bgcolor="#f9f9f9"><?php echo $row["gfmiscode"];?></td>
<td bgcolor="#f9f9f9"><?php  $date_re = explode("-",$row["register_date"]);
echo $date_re[2].'/'.$date_re[1].'/'.($date_re[0]+543);?></td>
<td bgcolor="#f9f9f9"><?php echo $row["unitprice"];?></td>
<td bgcolor="#f9f9f9"><?php echo $row["lifeperiod"];?></td>
<td bgcolor="#f9f9f9"><?php
$bud = $row["budgettype_id"];
$result_b = mysql_query("SELECT * FROM budgettypes WHERE id='$bud'")
 or die(mysql_error());
$row_b = mysql_fetch_array($result_b);
echo $row_b["description"];?></td>
<td bgcolor="#f9f9f9">
<?php
$yea = $row["year_id"];
$result_y = mysql_query("SELECT * FROM years WHERE id='$yea'")
 or die(mysql_error());
$row_y = mysql_fetch_array($result_y);
echo $row_y["year"];?>
</td>
<td bgcolor="#f9f9f9">
<?php
$rcv = $row["rcvtype_id"];
$result_r = mysql_query("SELECT * FROM rcvtypes WHERE id='$rcv'")
 or die(mysql_error());
$row_r = mysql_fetch_array($result_r);
echo $row_r["description"];?>
</td>
<td bgcolor="#f9f9f9">
<?php
$loc = $row["location_dept_id"];
$result_l = mysql_query("SELECT * FROM departments WHERE id='$loc'")
 or die(mysql_error());
$row_l = mysql_fetch_array($result_l);
echo $row_l["abbr"];?>
</td>
<td bgcolor="#f9f9f9">
<?php
$sta = $row["status_id"];
$result_s = mysql_query("SELECT * FROM statuses WHERE id='$sta'")
 or die(mysql_error());
$row_s = mysql_fetch_array($result_s);
echo $row_s["description"];?>
</td>
</tr>
</table>
<table><br>
<tr>
<td colspan="7" bgcolor="#f9f9f9"><b>ประวัติการโอนย้าย/แก้ไขรหัส</b></td>
</tr>
<tr>
<td><b>เลขทะเบียนครุภัณฑ์</b></td>
<td><b>รหัสครุภัณฑ์ใน GFMIS</b></td>
<td><b>วันที่ได้มา</b></td>
<td><b>ใช้ประจำที่สำนัก</b></td>
<td><b>วิธีการได้มา</b></td>
<td><b>หมายเหตุ</b></td>
<td></td>

</tr>
<tr>
<?php

$resulthis = mysql_query("SELECT * FROM assetshistories WHERE asset_id='".$row["id"]."'") 
 or die(mysql_error());
$rowhis = mysql_fetch_array($resulthis);
?>
<td bgcolor="#f9f9f9"><?php echo $rowhis["code2"];?></td>
<td bgcolor="#f9f9f9"><?php echo $rowhis["gfmiscode"];?></td>
<td bgcolor="#f9f9f9"><?php  $date_re = explode("-",$rowhis["register_date"]);
echo $date_re[2].'/'.$date_re[1].'/'.($date_re[0]+543);?></td>
<td bgcolor="#f9f9f9"><?php echo $rowhis["detail_location"];?></td>
<td bgcolor="#f9f9f9"><?php
$bud = $rowhis["location_dept_id"];
$result_b = mysql_query("SELECT * FROM budgettypes WHERE id='$bud'")
 or die(mysql_error());
$row_b = mysql_fetch_array($result_b);
echo $row_b["description"];?></td>
<td bgcolor="#f9f9f9"><?php echo $rowhis["remark"];?></td>
<td bgcolor="#f9f9f9"></td>
</tr>
</table>

    

  
      </div>  
 
  </form>


</body>
</html>
