<?php
error_reporting(0);
ob_start();
?>
<!doctype html public "-//w3c//dtd html 3.2//en">

<html>

<head>
<title></title>

<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 11px 28px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 4px 2px;
    cursor: pointer;
}
.button2 {border-radius: 4px;}

</style>
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    border: none;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>

<body>
<?php
include("config.php");
$strSQL = "SELECT * FROM prefixes";
$objQuery = mysql_query($strSQL) ;
$Num_Rows = mysql_num_rows($objQuery);
$Per_Page = 10;   // Per Page

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
	$Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
	$Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
	$Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
	$Num_Pages =($Num_Rows/$Per_Page)+1;
	$Num_Pages = (int)$Num_Pages;
}

$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
$objQuery  = mysql_query($strSQL);
?>




<div style="overflow-x:auto;">
  <table>
    <tr>
      <th><font color="#0099FF">#</font></th>
      <th><font color="#0099FF">ตัวย่อ</font></th>
      <th><font color="#0099FF">คำนำหน้า</font></th>
      <th><font color="#0099FF">ดำเนินการ</font></th>
    </tr>
    <?php
	$num1 = 0;
while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
      <td><?php echo $num1;?></td>
      <td><?php echo $objResult["abbr"];?></td>
      <td><?php echo $objResult["description"];?></td>
      <td><a href="main_prefixes_edit.php?preID=<?php echo $objResult["id"];?>"><img src="images/icons8-Edit-40.png" style="width:20px;height:20px;"></a><a href="JavaScript:if(confirm('Confirm Delete?')==true){window.location='prefixes_delete_detail.php?preID=<?php echo $objResult["id"];?>';}"><img src="images/icons8-Full Trash-40.png" style="width:20px;height:20px;"></a></td>
      

    </tr>
  <?php
	}
  ?>

  </table>
</div>

<a href="main_prefixes_insert"><button class="button button2">เพิ่มรายการ</button></a>
Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page :
<?php
if($Prev_Page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page'><< Back</a> ";
}

for($i=1; $i<=$Num_Pages; $i++){
	if($i != $Page)
	{
		echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i'>$i</a> ]";
	}
	else
	{
		echo "<b> $i </b>";
	}
}
if($Page!=$Num_Pages)
{
	echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page'>Next>></a> ";
}
?>
</body>

</html>
