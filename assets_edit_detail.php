<?php
	error_reporting(0);
	ob_start();
	date_default_timezone_set('Asia/Bangkok');
	include("config.php");
	header('Content-Type: text/html; charset=utf-8');

?>
<?php
 function valid($code4,$code5,$code1,$asset_des,$model,$gfmiscode,$unitprice,$unit_name,$lifeperiod,$asset_income,$rcvtypes,$budgettypes,$desp,$asset_location,$statuses,$start_date_new1,$code,$brands,$serial_no,$asset_remark,$departmentsold,$year_id,$asset_relation,$asset_sup,$projects,$catedate,$catedate,$name,$A_type,$A_location,$htype,$error)
 {
 ?>
<!DOCTYPE html>
<html>
    <script src="jquery-2.1.3.min.js"></script>

    <link href="bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="bootstrap-3.3.7-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <script src="bootstrap-3.3.7-dist/js/bootstrap.js"></script>

    <link href="dist/js1/datepicker.css" rel="stylesheet" />
    <script src="dist/js1/bootstrap-datepicker.js"></script>
    <script src="dist/js1/bootstrap-datepicker-thai.js"></script>
    <script src="dist/js1/bootstrap-datepicker.th.js" charset="UTF-8"></script>

    <script>
        $('.datepicker').datepicker();
    </script>
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    border: none;
    text-align: left;
    padding: 8px;
}

input[type=text], select ,textarea {
    width: 100%;
	padding: 5px 1px;
    margin: 1px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=submit] {
  
    background-color: #337ab7;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}
input[type=reset] {
   
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

</style>
<body>
 <?php

 if ($error != '')
 {
 echo '<div style="padding:4px; border:1px solid red; color:red;">'.$error.'</div>';
 }
$assID = $_GET['assID'];
include("config.php");
$result = mysql_query("SELECT * FROM assets WHERE id=$assID")
 or die(mysql_error());
$row = mysql_fetch_array($result);
 ?>


  <form action="assets_edit_detail.php" method="post" enctype="multipart/form-data">
  <div style="overflow-x:auto;">
    <table width="100%">
     <tr>
     <td width="17%" align="right" bgcolor="#d7ecf6">
     <label for="code" >หมวดหมู่ &nbsp;</label>
     </td>
     <td width="40%" bgcolor="#d7ecf6">
   
            <select name="code" >
			
			<?php  
			$categoriesId = $row["cat_id"];
			$strSQL = "SELECT * FROM categories ORDER BY id ASC";
			$objQuery = mysql_query($strSQL);
			while($objResuut = mysql_fetch_array($objQuery))
			{
			?>
			<option value="<?php echo $objResuut["code"];?>" <?php  if($categoriesId == $objResuut["id"]){ ?> selected <?php } ?>  ><?php echo $objResuut["code"];?> <?php echo $objResuut["description"];?></option>
			<?php
			}
			?>
		  </select>
     </td>
     <td  align="right" bgcolor="#d7ecf6">
     <label for="rcvdate">วันเริ่มต้น &nbsp;</label>
     </td>
     <td bgcolor="#d7ecf6">
          <?php 
      $beginA = $row["replace_date"];
      $beginAA = (explode("-",$beginA));
      $begindate31 = $beginAA[2]."/".$beginAA[1]."/".($beginAA[0]+543);
	 $date_start41 =  $begindate31;
	 ?>
  <!--   <input type="date" id="begindate" name="begindate"   value=""
      size="14" > -->
      <input class="input-medium" type="text" data-provide="datepicker" data-date-autoclose="true" name="begindate" data-date-language="th-th" size="14" value="<?php echo $date_start41 ?>">
     </td>
     </tr>
     </table>
     <table width="93%">
     <tr>
     <td width="16%" align="right" bgcolor="#d7ecf6">
     <label for="code1">เลขครุภัณฑ์ลำดับที่</label>
     </td>
     <td width="41%" bgcolor="#d7ecf6" >
     <input type="text" id="code1" name="code1" size="50" value="<?php echo $row["seq_no"];?>">
     <input type="hidden" id="asseid" name="asseid" value="<?php echo $row["id"];?>">
     </td>
     <td align="right" bgcolor="#d7ecf6">
     <label for="rcvdate">วันที่ได้มา &nbsp;</label>
     </td>
     <td bgcolor="#d7ecf6">
      <?php 
      $beginB = $row["register_date"];
      $beginBB = (explode("-",$beginB));
      $begindateA1 = $beginBB[2]."/".$beginBB[1]."/".($beginBB[0]+543);
	 $date_startA2 =  $begindateA1;
	 ?>
     
     <input class="input-medium" type="text" data-provide="datepicker" data-date-autoclose="true" name="rcvdate" data-date-language="th-th" size="20" value="<?php echo $date_startA2 ?>">
     </td>
     </tr>
     <tr>
     <td align="right" bgcolor="#d7ecf6">
      <label for="gfmiscode">รหัส GFMIS &nbsp;</label>
     </td>
     <td  bgcolor="#d7ecf6">
      <input type="text" id="gfmiscode" name="gfmiscode" size="50" value="<?php echo $row["gfmiscode"];?>">
     </td>
     <td align="right" bgcolor="#d7ecf6">
     <label for="years">ปีงบประมาณ &nbsp;</label>
     </td>
     <td bgcolor="#d7ecf6">
            <select name="years" >
			<?php
			$year_id1 = $row["year_id"];
			$strSQL1 = "SELECT * FROM years ORDER BY year DESC";
			$objQuery1 = mysql_query($strSQL1);
			while($objResuut1 = mysql_fetch_array($objQuery1))
			{
			?>
			<option value="<?php echo $objResuut1["year"];?>"  <?php  if($year_id1 == $objResuut1["id"]){ ?> selected <?php } ?> ><?php echo $objResuut1["year"];?></option>
			<?php
			}
			?>
		  </select>
     </td>
     </tr>

      </table>
      <table width="100%">
     <tr>
     <td width="173" bgcolor="#fcf8e3"><label for="unitprice">ราคาต่อหน่วย(บาท)</label></td>
     <td width="192" bgcolor="#fcf8e3"><label for="lifeperiod">อายุการใช้งาน(ปี)</label></td>
     <td width="206" bgcolor="#fcf8e3"><label for="budgettypes">แหล่งงบ</label></td>
     <td width="265" bgcolor="#fcf8e3"><label for="asset_sup">แหล่งที่มา(Supplier)</label></td>
     <td width="256" bgcolor="#fcf8e3"><label for="rcvtypes">วิธีการได้มา</label></td>
     <td width="124" bgcolor="#fcf8e3"><label for="rcvtypes">การแจ้งซ่อม</label></td>
     
     </tr>
     <tr>
     <td bgcolor="#f9f9f9"><input type="text" id="unitprice" name="unitprice" size="30" value="<?php echo $row["unitprice"];?>"></td>
     <td bgcolor="#f9f9f9"><input type="text" id="lifeperiod" name="lifeperiod" size="30" value="<?php echo $row["lifeperiod"];?>"></td>
     <td bgcolor="#f9f9f9"><select name="budgettypes" >
			<?php
			$budgettype_id1 = $row["budgettype_id"];
			$strSQL2 = "SELECT * FROM budgettypes ORDER BY id ASC";
			$objQuery2 = mysql_query($strSQL2);
			while($objResuut2 = mysql_fetch_array($objQuery2))
			{
			?>
			<option value="<?php echo $objResuut2["id"];?>" <?php  if($budgettype_id1 == $objResuut2["id"]){ ?> selected <?php } ?>><?php echo $objResuut2["description"];?></option>
			<?php
			}
			?>
		  </select></td>
      <td bgcolor="#f9f9f9"><select name="asset_sup" >
			<?php
			$location_dept_id1 = $row["supplier_name"];
			$strSQL11 = "SELECT * FROM locations ORDER BY id ASC";
			$objQuery11 = mysql_query($strSQL11);
			while($objResuut11 = mysql_fetch_array($objQuery11))
			{
			?>
			<option value="<?php echo $objResuut11["id"];?>" <?php  if($location_dept_id1 == $objResuut11["id"]){ ?> selected <?php } ?>><?php echo $objResuut11["description"];?></option>
			<?php
			}
			?>
		  </select></td>
          <td bgcolor="#f9f9f9">
          <select name="rcvtypes" >
			<?php
			$rcvtype_id1 = $row["rcvtype_id"];
			$strSQL3 = "SELECT * FROM rcvtypes ORDER BY id ASC";
			$objQuery3 = mysql_query($strSQL3);
			while($objResuut3 = mysql_fetch_array($objQuery3))
			{
			?>
			<option value="<?php echo $objResuut3["id"];?>" <?php  if($rcvtype_id1 == $objResuut3["id"]){ ?> selected <?php } ?>><?php echo $objResuut3["description"];?></option>
			<?php
			}
			?>
		  </select>
          </td>
          <td bgcolor="#f9f9f9">
          <select name="htype" >
			<?php
			$htype = $row["htype"];
			$strhtype = "SELECT * FROM helpdesk ORDER BY id ASC";
			$objQueryhtype = mysql_query($strhtype);
			while($objResuuthtype = mysql_fetch_array($objQueryhtype))
			{
			?>
			<option value="<?php echo $objResuuthtype["id"];?>" <?php  if($htype == $objResuuthtype["id"]){ ?> selected <?php } ?>><?php echo $objResuuthtype["description"];?></option>
			<?php
			}
			?>
		  </select>
          </td>
     </tr>
    </table>
      <table width="100%">
      <tr>
     <td width="177" bgcolor="#d9edf7"><label for="brands">ยี่ห้อ</label></td>
     <td width="213" bgcolor="#d9edf7"><label for="model">รุ่น/แบบ(Model)</label></td>
     <td width="138" bgcolor="#d9edf7"><label for="A_type">ประเภท</label></td>
     <td width="138" bgcolor="#d9edf7"><label for="A_location">สถานที่ตั้ง</label></td>
     <td width="204" bgcolor="#d9edf7"><label for="serial_no">เลขประจำตัว(Serial no.)</label></td>
     <td width="222" bgcolor="#d9edf7"><label for="unit_name">หน่วย</label></td>
     <td width="222" bgcolor="#d9edf7"><label for="file">รูปครุภัณฑ์</label></td> 
      </tr>
      <tr>
     <td width="177" bgcolor="#f9f9f9"><select name="brands">
			<?php
			$brand_id1 = $row["brand_id"];
			$strSQL4 = "SELECT * FROM brands ORDER BY id ASC";
			$objQuery4 = mysql_query($strSQL4);
			while($objResuut4 = mysql_fetch_array($objQuery4))
			{
			?>
			<option value="<?php echo $objResuut4["id"];?>" <?php  if($brand_id1 == $objResuut4["id"]){ ?> selected <?php } ?>><?php echo $objResuut4["description"];?></option>
			<?php
			}
			?>
		  </select></td>
     <td width="213" bgcolor="#f9f9f9"><input type="text" id="model" name="model" value="<?php echo $row["model"];?>"></td>
          <td width="138" bgcolor="#f9f9f9">
     <select name="A_type">
			<?php
			$code_type1 = $row["code_type"];
			$strSQLA = "SELECT * FROM catetype ORDER BY id ASC";
			$objQueryA = mysql_query($strSQLA);
			while($objResuutA = mysql_fetch_array($objQueryA))
			{
			?>
			<option value="<?php echo $objResuutA["id"];?>" <?php  if($code_type1 == $objResuutA["id"]){ ?> selected <?php } ?>><?php echo $objResuutA["description"];?></option>
			<?php
			}
			?>
		  </select>
     </td>
     <td width="138" bgcolor="#f9f9f9">
          <select name="A_location">
			<?php
			$place_type1 = $row["place_type"];
			$strSQLP = "SELECT * FROM placetype ORDER BY id ASC";
			$objQueryP = mysql_query($strSQLP);
			while($objResuutP = mysql_fetch_array($objQueryP))
			{
			?>
			<option value="<?php echo $objResuutP["id"];?>" <?php  if($place_type1 == $objResuutP["id"]){ ?> selected <?php } ?>><?php echo $objResuutP["description"];?></option>
			<?php
			}
			?>
		  </select>
     
     </td>
     <td width="204" bgcolor="#f9f9f9"><input type="text" id="serial_no" name="serial_no" value="<?php echo $row["serial_no"];?>"></td>
     <td width="222" bgcolor="#f9f9f9"><input type="text" id="unit_name" name="unit_name" value="<?php echo $row["unit_name"];?>"></td>
     <td width="256" bgcolor="#f9f9f9"><input type="file" name="fileUpload" id="fileUpload" value="<?php echo $row["img"]; ?>"> </td>
      </tr>
    </table>
      <table>
      <tr>
     <td width="131" bgcolor="#f2dede"><label for="departments">ใช้ประจำที่สำนัก</label></td>
     <td width="136" bgcolor="#f2dede"><label for="departmentsold">สำนัก(ผู้ถือครอง)</label></td>
     <td width="174" bgcolor="#f2dede"><label for="users">ชื่อ-สกุล(ผู้ถือครอง)</label></td>
     <td width="299" bgcolor="#f2dede"><label for="projects">รหัสโครงการ</label></td>
     <td width="110" bgcolor="#f2dede"><label for="statuses">สถานะ</label></td>
     <td width="218" bgcolor="#f2dede"><label for="code4">ทดแทนครุภัณฑ์เลขที่</label></td>
      </tr>
      <tr>
     <td width="131" bgcolor="#f9f9f9"> <select name="departments" >
			<?php
			$location_dept_id1 = $row["location_dept_id"];
			$strSQL5 = "SELECT * FROM departments ORDER BY id ASC";
			$objQuery5 = mysql_query($strSQL5);
			while($objResuut5 = mysql_fetch_array($objQuery5))
			{
			?>
			<option value="<?php echo $objResuut5["abbr"];?>" <?php  if($location_dept_id1 == $objResuut5["id"]){ ?> selected <?php } ?>><?php echo $objResuut5["abbr"];?></option>
			<?php
			}
			?>
		  </select></td>
     <td width="136" bgcolor="#f9f9f9"><select name="departmentsold" >
			<?php
			$owner_dept_id1 = $row["owner_dept_id"];
			$strSQL6 = "SELECT * FROM departments ORDER BY id ASC";
			$objQuery6 = mysql_query($strSQL6);
			while($objResuut6 = mysql_fetch_array($objQuery6))
			{
			?>
			<option value="<?php echo $objResuut6["abbr"];?>" <?php  if($owner_dept_id1 == $objResuut6["id"]){ ?> selected <?php } ?>><?php echo $objResuut6["abbr"];?></option>
			<?php
			}
			?>
		  </select></td>
     <td width="174" bgcolor="#f9f9f9"> <select name="users" >
			<?php
			$owner_user_id1 = $row["owner_user_id"];
			$strSQL7 = "SELECT * FROM users ORDER BY id ASC";
			$objQuery7 = mysql_query($strSQL7);
			while($objResuut7 = mysql_fetch_array($objQuery7))
			{
			?>
			<option value="<?php echo $objResuut7["id"];?>" <?php  if($owner_user_id1 == $objResuut7["id"]){ ?> selected <?php } ?>><?php echo $objResuut7["name"];?> <?php echo $objResuut7["lname"];?></option>
			<?php
			}
			?>
		  </select></td>
     <td width="299" bgcolor="#f9f9f9"> <select name="projects" >
			<?php
			$prj_id1 = $row["prj_id"];
			$strSQL8 = "SELECT * FROM projects ORDER BY id ASC";
			$objQuery8 = mysql_query($strSQL8);
			while($objResuut8 = mysql_fetch_array($objQuery8))
			{
			?>
			<option value="<?php echo $objResuut8["id"];?>" <?php  if($prj_id1 == $objResuut8["id"]){ ?> selected <?php } ?>><?php echo $objResuut8["name"];?></option>
			<?php
			}
			?>
		  </select></td>
          <td>
                  <select name="statuses">
			<?php
			$status_id1 = $row["status_id"];
			$strSQL9 = "SELECT * FROM statuses ORDER BY id ASC";
			$objQuery9 = mysql_query($strSQL9);
			while($objResuut9 = mysql_fetch_array($objQuery9))
			{
			?>
			<option value="<?php echo $objResuut9["id"];?>" <?php  if($status_id1 == $objResuut9["id"]){ ?> selected <?php } ?>><?php echo $objResuut9["description"];?></option>
			<?php
			}
			?>
		  </select>
          </td>
          
          <td>
           <select name="code4">
           <option value="">-</option>
			<?php
			$strSQL10 = "SELECT * FROM assets ORDER BY id ASC";
			$objQuery10 = mysql_query($strSQL10);
			while($objResuut10 = mysql_fetch_array($objQuery10))
			{
			?>
			<option value="<?php echo $objResuut10["id"];?>"><?php echo $objResuut10["code2"];?></option>
			<?php
			}
			?>
		  </select>
          </td>
      </tr>
      </table>
      <table>
      <tr>
     <td width="206" bgcolor="#dff0d8"><label for="asset_des">รายละเอียดครุภัณฑ์</label></td>
     <td width="194" bgcolor="#dff0d8"><label for="asset_income">รายละเอียดการได้มา</label></td>
     <td width="293" bgcolor="#dff0d8"><label for="asset_location">รายละเอียดที่ตั้ง</label></td>
     <td width="172" bgcolor="#dff0d8"> <label for="asset_relation">เอกสารที่เกี่ยวข้อง</label></td>
     
     </tr>
     <tr>
     <td width="206" bgcolor="#f9f9f9"> <textarea id="asset_des" name="asset_des"><?php echo $row["detail"];?></textarea></td>
     <td width="194" bgcolor="#f9f9f9"><textarea id="asset_income" name="asset_income" ><?php echo $row["detail_rcv"];?></textarea></td>
     <td width="293" bgcolor="#f9f9f9"><textarea id="asset_location" name="asset_location" ><?php echo $row["detail_location"];?></textarea></td>
     <td width="172" bgcolor="#f9f9f9"><textarea id="asset_relation" name="asset_relation" ><?php echo $row["doc_ref"];?></textarea></td>
     
      </tr>
      </table>
      <table>
      <tr>
      <td width="34%" bgcolor="#fcf8e3"><label for="asset_remark">หมายเหตุ</label></td>
      <td width="33%" bgcolor="#fcf8e3"><label for="asset_remark">เอกสารเพิ่มเติม</label></td>
      <td width="33%" bgcolor="#fcf8e3"><label for="asset_remark">ดาวน์โหลดเอกสาร</label></td>
      </tr>
      <tr>
     <td>
      <textarea id="asset_remark" name="asset_remark" ><?php echo $row["remark"];?></textarea>
      </td>
      <td>
      <input type="file" name="fileother" id="fileother"/>
      </td>
      <td>
      
      <?php 
	  if ($row["imageother"] =="")
	  {
		echo "";  
	  }
	  else
	  {
		  ?>
          <a href=<?php echo "images/".$row["imageother"]; ?>><a href=<?php echo "images/".$row["imageother"]; ?>><img src="images/folder.png" style="width:40px;height:40px;" title="download"></a>
       <?php
	
	  }
	 
	  ?>
      </td>
      </tr>
      </table>   
      </div>  
  <input type="submit" name="submit" value="บันทึก" width="10%">
  </form>


</body>
</html>
 <?php

}



 if (isset($_POST['submit']))
 {

 $code = explode(".",mysql_real_escape_string(htmlspecialchars($_POST['code'])));
 $code2 = mysql_real_escape_string(htmlspecialchars($_POST['code']));
 $code1 = mysql_real_escape_string(htmlspecialchars($_POST['code1']));
 $A_type = mysql_real_escape_string(htmlspecialchars($_POST['A_type']));
 $A_location = mysql_real_escape_string(htmlspecialchars($_POST['A_location']));
 $gfmiscode = mysql_real_escape_string(htmlspecialchars($_POST['gfmiscode']));

 $years = substr(mysql_real_escape_string(htmlspecialchars($_POST['years'])),2);
 $years1 = mysql_real_escape_string(htmlspecialchars($_POST['years']));
 $unitprice = mysql_real_escape_string(htmlspecialchars($_POST['unitprice']));
 $lifeperiod = mysql_real_escape_string(htmlspecialchars($_POST['lifeperiod']));
 $budgettypes = mysql_real_escape_string(htmlspecialchars($_POST['budgettypes']));
  $rcvtypes = mysql_real_escape_string(htmlspecialchars($_POST['rcvtypes']));
 $brands = mysql_real_escape_string(htmlspecialchars($_POST['brands']));
 $model = mysql_real_escape_string(htmlspecialchars($_POST['model']));
 $serial_no = mysql_real_escape_string(htmlspecialchars($_POST['serial_no']));
 $unit_name = mysql_real_escape_string(htmlspecialchars($_POST['unit_name']));
 //$file = mysql_real_escape_string(htmlspecialchars($_POST['photo']));
 $departments = mysql_real_escape_string(htmlspecialchars($_POST['departments']));
 
 $code11 =  $years.'-'.$code2.'-'.$code1;
 //$code5 = $departments.$years.'/'.$code[0].'/'.$code2.'/'.$code1;
 //exit();
 $departmentsold = mysql_real_escape_string(htmlspecialchars($_POST['departmentsold']));
 $users = mysql_real_escape_string(htmlspecialchars($_POST['users']));
 $projects = mysql_real_escape_string(htmlspecialchars($_POST['projects']));
 $statuses = mysql_real_escape_string(htmlspecialchars($_POST['statuses']));
 $code4 = mysql_real_escape_string(htmlspecialchars($_POST['code4']));
 $asset_des = mysql_real_escape_string(htmlspecialchars($_POST['asset_des']));
 $asset_income = mysql_real_escape_string(htmlspecialchars($_POST['asset_income']));
 $asset_location = mysql_real_escape_string(htmlspecialchars($_POST['asset_location']));
 $asset_relation = mysql_real_escape_string(htmlspecialchars($_POST['asset_relation']));
 $asset_sup = mysql_real_escape_string(htmlspecialchars($_POST['asset_sup']));
 $asset_remark = mysql_real_escape_string(htmlspecialchars($_POST['asset_remark']));
 $asseid = mysql_real_escape_string(htmlspecialchars($_POST['asseid']));
 $begindate = mysql_real_escape_string(htmlspecialchars($_POST['begindate']));
 $htype = mysql_real_escape_string(htmlspecialchars($_POST['htype']));

 $result_d = mysql_query("SELECT * FROM departments WHERE abbr='$departments'")
 or die(mysql_error());
 $row_d = mysql_fetch_array($result_d);
 $desp = $row_d["id"];
 
  $result_de_old = mysql_query("SELECT * FROM departments WHERE abbr ='$departmentsold'")
 or die(mysql_error());
 $row_de_old = mysql_fetch_array($result_de_old);
 $d_de_old = $row_de_old["id"];

 
 $result_c = mysql_query("SELECT * FROM categories WHERE code ='$code2'") or die(mysql_error());
while($objResuut_C = mysql_fetch_array($result_c))
{
	$cate_id =  $objResuut_C["id"];
	$cate_id22 =  $objResuut_C["code"];
 
}
 //echo  $cate_id;
//exit();

if ($code[2]=="")
{
   $code5 = $departmentsold.$years.'/'. $code[0].'/'.$code[0].'.'.$code[1].'/'.$code1;
  // echo 	$code5;
  // exit();
}
else
{
	$code5 = $departmentsold.$years.'/'. $code[0].'/'.$code[0].'.'.$code[1].'.'.$code[2].'/'.$code1;
	//echo 	$code5;
	// exit();
}

// $code5 = $departmentsold.$years.'/'. $code[0].'/'.$code[1].'/'.$code1;
 
 $result_y = mysql_query("SELECT * FROM years WHERE year ='$years1'")
 or die(mysql_error());
 $row_y = mysql_fetch_array($result_y);
 $year_id = $row_y["id"];
 
 $start_date = mysql_real_escape_string(htmlspecialchars($_POST['rcvdate']));
 $start_date_new = (explode("/",$start_date));
 $start_date_new1 = ($start_date_new[2]-543)."-".$start_date_new[1]."-".$start_date_new[0];
 
 $start_be = mysql_real_escape_string(htmlspecialchars($_POST['begindate']));
 $start_date_be = (explode("/",$start_be));
 $start_date_be = ($start_date_be[2]-543)."-".$start_date_be[1]."-".$start_date_be[0];

 include "BarcodeQR.php"; 

 $qr = new BarcodeQR(); 
 

 $filenameqr = $asseid. '.png';
 $filenameqr1 = "main_assets_tranfer_view.php?assID=".$asseid;
$images_other = $_FILES["fileother"]["name"];
//copy($_FILES["fileother"]["tmp_name"],"images/".$_FILES["fileother"]["name"]);
move_uploaded_file($_FILES["fileother"]["tmp_name"],"images/".$_FILES["fileother"]["name"]);


 
 $qr->url($filenameqr1); 
 $qr->draw(200, "qrcode/".$filenameqr);
 if ($gfmiscode == '' )
 {

 $error = 'Please enter the details!';

 valid($code4,$code5,$code1,$asset_des,$model,$gfmiscode,$unitprice,$unit_name,$lifeperiod,$asset_income,$rcvtypes,$budgettypes,$desp,$asset_location,$statuses,$start_date_new1,$code,$brands,$serial_no,$asset_remark,$departmentsold,$year_id,$asset_relation,$asset_sup,$projects,$catedate,$catedate,$name,$A_type,$A_location,$htype,$error);
 }
 else
 {

	 		



		$sur1 = strrchr($_FILES["fileUpload"]["name"]);
		$new_images =  (Date("dmy_His").$sur1); 
		move_uploaded_file($_FILES["fileUpload"]["tmp_name"],"myfile/".$new_images) ;
        $catedate =date('Y-m-d H:i:s');
							 define("MAX_SIZE" , 500000);
					         define("MAX_SIZE1" , 500000);
					 if($_FILES['fileother']['size']>0 && $_FILES['fileother']['size']>MAX_SIZE){
						  echo "ขนาดรูปใหญ่เกินกว่า 5 MB<br><br>";
						  exit();
						}
					if ($_FILES['fileUpload']['size']>0 && $_FILES['fileUpload']['size']>MAX_SIZE1) //ตรวจสอบขนาด
					 {
					 	  echo "ขนาดรูปใหญ่เกินกว่า 5 MB<br><br>";
						   exit();
					 } 	
		$sql = "UPDATE assets SET code1='$code11',code2='$code5',seq_no ='$code1',detail='$asset_des',model='$model',gfmiscode='$gfmiscode',unitprice='$unitprice',unit_name='$unit_name',lifeperiod='$lifeperiod',detail_rcv='$asset_income',rcvtype_id='$rcvtypes',budgettype_id='$budgettypes',location_dept_id='$desp',detail_location='$asset_location',status_id='$statuses',register_date='$start_date_new1',cat_id='$cate_id',brand_id='$brands',serial_no='$serial_no',useable='Y',remark='$asset_remark',owner_user_id='$users',owner_dept_id='$d_de_old',year_id='$year_id',doc_ref='$asset_relation',replace_date='$start_date_be',supplier_name='$asset_sup',prj_id='$projects',created_at='$catedate' ,updated_at='$catedate',alive_flag= '1',code_type='$A_type',htype='$htype',place_type='$A_location',qrcode='$filenameqr'";
		$sql .= (trim($_FILES["fileUpload"]["tmp_name"]) != "")? " ,img='".$new_images."'": "";
		$sql .= (trim($_FILES["fileother"]["tmp_name"]) != "")? " ,imageother='".$images_other."'":"";
		$sql .= " where id='$asseid'";
		mysql_query($sql);




 header("Location: AssetsManage.php");

 }
 }
 else
 {
 valid('','','','');
 }
?>