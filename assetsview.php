<?php
	error_reporting(0);
	session_start();
	ob_start();


date_default_timezone_set('Asia/Bangkok');

/** PHPExcel */
require_once 'Classes/PHPExcel.php';
?>
<!doctype html public "-//w3c//dtd html 3.2//en">

<html>

<head>
<title></title>

<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 11px 28px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 4px 2px;
    cursor: pointer;
}
.button2 {border-radius: 4px;}

</style>
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}
input[type=text], select ,textarea {
    width: 100%; 
	padding: 5px 1px;
    margin: 1px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=submit] {
  
    background-color: #337ab7;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}
th, td {
    border: none;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>

<body>

<form name="frmSearch" method="get" action="<?php echo $_SERVER['SCRIPT_NAME'];?>">

<?php
include("config.php");


?>

<div style="overflow-x:auto;">
<table>
<tr>
<td>เลขครุภัณฑ์</td>
<td>ปีงบประมาณ</td>
<td>สถานะ</td>
<td>ใช้ประจำที่สำนัก</td>
<td>ผู้ถือครอง</td>
</tr>
<tr>
<td bgcolor="#f9f9f9"><input type="text" id="code1" name="code1" size="15"></td>
<td bgcolor="#f9f9f9"><select name="year">
            <option value="">เลือกปีงบประมาณ</option>
			<?php
			$strSQL1 = "SELECT * FROM years ORDER BY year DESC";
			$objQuery1 = mysql_query($strSQL1);
			while($objResuut1 = mysql_fetch_array($objQuery1))
			{
			?>
			<option value="<?php echo $objResuut1["id"];?>"><?php echo $objResuut1["year"];?></option>
			<?php
			}
			?>
		  </select>
		  </td>
<td bgcolor="#f9f9f9"><select name="statuses">
            <option value="">เลือกสถานะ</option>
			<?php
			$strSQL9 = "SELECT * FROM statuses ORDER BY id ASC";
			$objQuery9 = mysql_query($strSQL9);
			while($objResuut9 = mysql_fetch_array($objQuery9))
			{
			?>
			<option value="<?php echo $objResuut9["id"];?>"><?php echo $objResuut9["description"];?></option>
			<?php
			}
			?>
		  </select></td>
<td bgcolor="#f9f9f9"><select name="departments" >
            <option value="">เลือกที่ตั้ง</option>
			<?php
			$strSQL5 = "SELECT * FROM departments ORDER BY id ASC";
			$objQuery5 = mysql_query($strSQL5);
			while($objResuut5 = mysql_fetch_array($objQuery5))
			{
			?>
			<option value="<?php echo $objResuut5["id"];?>"><?php echo $objResuut5["abbr"];?></option>
			<?php
			}
			?>
		  </select></td>
<td bgcolor="#f9f9f9"><select name="users" >
            <option value="">เลือกผู้ถือครอง</option>
			<?php
			$strSQL7 = "SELECT * FROM users ORDER BY id ASC";
			$objQuery7 = mysql_query($strSQL7);
			while($objResuut7 = mysql_fetch_array($objQuery7))
			{
			?>
			<option value="<?php echo $objResuut7["id"];?>"><?php echo $objResuut7["name"];?> <?php echo $objResuut7["lname"];?></option>
			<?php
			}
			?>
		  </select></td>
          
</tr>
<tr>
<td><select name="A_Type" >
            <option value="">เลือกประเภท</option>
			<?php
			$strSQLC = "SELECT * FROM catetype ORDER BY id ASC";
			$objQueryC = mysql_query($strSQLC);
			while($objResuutC = mysql_fetch_array($objQueryC))
			{
			?>
			<option value="<?php echo $objResuutC["id"];?>"><?php echo $objResuutC["description"];?> </option>
			<?php
			}
			?>
		  </select></td>
    <td><select name="A_location" >
            <option value="">เลือกสถานที่ตั้ง</option>
			<?php
			$strSQLPt = "SELECT * FROM placetype ORDER BY id ASC";
			$objQueryPt = mysql_query($strSQLPt);
			while($objResuutPt = mysql_fetch_array($objQueryPt))
			{
			?>
			<option value="<?php echo $objResuutPt["id"];?>"><?php echo $objResuutPt["description"];?> </option>
			<?php
			}
			?>
		  </select></td>
             
          <td>
          <select name="A_Project"  style="width: 250px;">
            <option value="">เลือกโครงการ</option>
			<?php
			$strSQLPt = "SELECT * FROM projects ORDER BY id ASC";
			$objQueryPt = mysql_query($strSQLPt);
			while($objResuutPt = mysql_fetch_array($objQueryPt))
			{
			?>
			<option value="<?php echo $objResuutPt["id"];?>" width="300px"><?php echo $objResuutPt["name"];?> </option>
			<?php
			}
			?>
		  </select>
          </td>
          <td>
          
          </td>
           <td><input type="submit" value="ค้นหา" width="20%">
          </td>
</tr>
<tr>
<td></td>
</form>
<?php

   $strSQL = "SELECT * FROM assets";

if (($_GET["year"] != "") && ($_GET["departments"] != "")  )
	{
		
	$strSQL .= " WHERE year_id ='".$_GET["year"]."'  and location_dept_id ='".$_GET["departments"]."' and alive_flag <>2";
	
	}

	
elseif ($_GET["year"] != "")
{
	
$strSQL .= " WHERE (year_id ='".$_GET["year"]."') and alive_flag <>2";
}
elseif ($_GET["statuses"] != "")
{
	$strSQL .= " WHERE (status_id ='".$_GET["statuses"]."') and alive_flag <>2";
}
elseif ($_GET["departments"] != "")
{
	$strSQL .= " WHERE(location_dept_id ='".$_GET["departments"]."') and alive_flag <>2"; 
	
}
elseif ($_GET["users"] != "")
{
	$strSQL .= " WHERE (owner_user_id ='".$_GET["users"]."') and alive_flag <>2";
}
elseif ($_GET["A_Project"] != "")
{
	$strSQL .= " WHERE (prj_id ='".$_GET["A_Project"]."') and alive_flag <>2";
}
elseif ($_GET["code1"] != "")
{
	
	//echo $_GET["code1"];
	//exit();
	
	$strSQL0 = "SELECT Rcode FROM assetshistories WHERE (code2 ='".$_GET["code1"]."')";
	$objQuery0 = mysql_query($strSQL0) or die ("Error Query [".$strSQL0."]");
	//$row_code0 = mysql_fetch_row($objQuery0);
	
	while($row_code0 = mysql_fetch_array($objQuery0))
			{
				$row_code1 =  $row_code0["Rcode"];
			}
	
	//echo $row_code1;
	//exit();
	$strSQL = "SELECT * FROM assets ";
		 if ($row_code1=="")
	 {
		 $strSQL .= "WHERE (code2 ='".$_GET["code1"]."') and alive_flag <>2 ";	 
	
	}
	 else
	 {
		// echo "dddd";
	//exit();
		$strSQL .= "WHERE (Rcode ='$row_code1') and alive_flag <>2 ";
	 }

	
}

elseif ($_GET["A_Type"] != "")
{
$strSQL .= " WHERE (code_type ='".$_GET["A_Type"]."') and alive_flag <>2";	
	
}
elseif ($_GET["A_location"] != "")
{
$strSQL .= " WHERE (place_type ='".$_GET["A_location"]."') and alive_flag <>2";	
	
}


	$objQuery = mysql_query($strSQL)or die ("Error Query [".$strSQL."]");
	$Num_Rows = mysql_num_rows($objQuery);
	//}
	
	$Per_Page = 30;   // Per Page

	$Page = $_GET["Page"];
	if(!$_GET["Page"])
	{
		$Page=1;
	}

	$Prev_Page = $Page-1;
	$Next_Page = $Page+1;

	$Page_Start = (($Per_Page*$Page)-$Per_Page);
	if($Num_Rows<=$Per_Page)
	{
		$Num_Pages =1;
	}
	else if(($Num_Rows % $Per_Page)==0)
	{
		$Num_Pages =($Num_Rows/$Per_Page) ;
	}
	else
	{
		$Num_Pages =($Num_Rows/$Per_Page)+1;
		$Num_Pages = (int)$Num_Pages;
	}
	$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
	$objQuery  = mysql_query($strSQL);
?>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table> <br>
  <table>
    <tr>
      <th width="5%"><font color="#0099FF">#</font></th>
      <th width="10%"><font color="#0099FF">เลขทะเบียน</font></th>
      <th width="18%"><font color="#0099FF">รายการ</font></th>
      <th width="12%"><font color="#0099FF">รหัส GFMIS</font></th>
      <th width="8%"><font color="#0099FF">ราคาต่อหน่วย</font></th>
      <th width="11%"><font color="#0099FF">ใช้ประจำที่</font></th>
      <th width="10%"><font color="#0099FF">ประเภท</font></th>
      <th width="10%"><font color="#0099FF">สถานที่ตั้ง</font></th>
      <th width="13%"><font color="#0099FF">ดำเนินการ</font></th>
    </tr>
    <?php
	  $objPHPExcel = new PHPExcel();

// Set properties

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


// Add some data



$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'เลขครุภัณฑ์')
            ->setCellValue('B1', 'รายการ')
            ->setCellValue('C1', 'gfmis')
			->setCellValue('D1', 'ราคา')
			->setCellValue('E1', 'หน่วย')
            ->setCellValue('F1', 'สถานที่ใช้')
			->setCellValue('G1', 'ผู้ใช้งาน')
			->setCellValue('H1', 'โครงการ')
			->setCellValue('I1', 'วันที่ได้มา');
	
	
	$i = 2;
	$num1 = 0;
while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
      <td><?php echo $num1;?></td>
      <td><a href="main_assets_tranfer_view.php?assID=<?php echo $objResult["id"];?>">
	  
	  
	  <?php echo $objResult["code2"];
	  
	  
	  
	  ?></a></td>
      <td><?php echo $objResult["detail"];?></td>
      <td><?php echo $objResult["gfmiscode"];?></td>
      <td><?php echo number_format($objResult["unitprice"], 2);?></td>
      <td><?php 
	  
	  //echo $objResult["location_dept_id"];
	  $location_dept_id = $objResult["location_dept_id"];
$result_lo = mysql_query("SELECT * FROM departments WHERE id='$location_dept_id'")
 or die(mysql_error());
$row_lo = mysql_fetch_array($result_lo);
echo $row_lo["abbr"];
	  
	  ?></td>
        <td><?php 
		$A_Type = $objResult["code_type"];
		$result_Type = mysql_query("SELECT * FROM catetype WHERE id ='$A_Type'")
		 or die(mysql_error());
		$row_Type = mysql_fetch_array($result_Type);
		echo $row_Type["description"];
	  
	  ?></td>
            <td><?php 
			
			$A_location = $objResult["place_type"];
			$result_location = mysql_query("SELECT * FROM placetype WHERE id='$A_location'")
			 or die(mysql_error());
			$row_location = mysql_fetch_array($result_location);
			echo $row_location["description"];
	  
	  ?></td>
      <td><a href="main_assets_edit.php?assID=<?php echo $objResult["id"];?>"><img src="images/icons8-Edit-40.png" style="width:20px;height:20px;" title="แก้ไข"></a><a href="JavaScript:if(confirm('Confirm Delete?')==true){window.location='assets_delete_detail.php?assID=<?php echo $objResult["id"];?>';}"><img src="images/icons8-Full Trash-40.png" style="width:20px;height:20px;" title="ลบ"></a><a href="main_assets_edit_two.php?asssID=<?php echo $objResult["id"];?>"><img src="images/Cloud-48.png" style="width:30px;height:30px;" title="โอนย้าย"></a><a href="main_assets_control_view.php?assID=<?php echo $objResult["id"];?>"><img src="images/icons8-Document-48.png" style="width:23px;height:22px;" title="ทะเบียนคุมทรัพย์สิน"></a></td>
      

    </tr>
  <?php
  
           $strSQL5 = "SELECT * FROM departments where id = '".$objResult["location_dept_id"]."'";
			$objQuery5 = mysql_query($strSQL5);
			while($objResuut5 = mysql_fetch_array($objQuery5))
			{
				$location  =  $objResuut5["abbr"];
			
			}
			$strSQLUser = "SELECT * FROM users where id = '".$objResult["owner_user_id"]."'";
			$objQueryUser = mysql_query($strSQLUser);
			while($objResuutUser = mysql_fetch_array($objQueryUser))
			{
				$UserA  =  $objResuutUser["name"].' '.$objResuutUser["lname"];
			
			}
			$strSQLprojects = "SELECT * FROM projects where id = '".$objResult["prj_id"]."'";
			$objQueryprojects = mysql_query($strSQLprojects);
			while($objResuutprojects = mysql_fetch_array($objQueryprojects))
			{
				$projects  =  $objResuutprojects["name"];
			
			}

			
			   $beginB = $objResult["register_date"];
			  $beginBB = (explode("-",$beginB));
			  $begindateA1 = $beginBB[2]."/".$beginBB[1]."/".($beginBB[0]+543);
			 
  
  	$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $objResult["code2"]);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth("20");
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('333300');
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $objResult["detail"]);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth("60");
	$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $objResult["gfmiscode"], PHPExcel_Cell_DataType::TYPE_STRING);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth("15");
	$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $objResult["unitprice"]);
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $objResult["unit_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $location);
	$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $UserA);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth("20");
	$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $projects);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth("50");
	$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $begindateA1);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth("15");
	
  
  $i++;
  
	}
  ?>

  </table>
  <?php
  
$objPHPExcel->getActiveSheet()->setTitle('รายการครุภัณฑ์');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Save Excel 2007 file

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$strFileName = "FileAssets.xls";
$objWriter->save($strFileName);

  
  
  
?>
  
</div>

<a href="AssetsNew.php"><img src="images/add-1.png" style="width:40px;height:40px;" title="เพิ่ม"> </a>&nbsp;<a href=<?php echo $strFileName; ?>><img src="images/excel.png" style="width:40px;height:40px;" title="Export"> </a>
Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page : 
	<?php
	if($Prev_Page)
	{
		echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page&year=$_GET[year]&statuses=$_GET[statuses]&departments=$_GET[departments]'><< Back</a> ";
	}

	for($i=1; $i<=$Num_Pages; $i++){
		if($i != $Page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i&year=$_GET[year]&statuses=$_GET[statuses]&departments=$_GET[departments]'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
	if($Page!=$Num_Pages)
	{
		echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page&year=$_GET[year]&statuses=$_GET[statuses]&departments=$_GET[departments]'>Next>></a> ";
	}

	
		
	?>


</body>

</html>
