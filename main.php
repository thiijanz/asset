<?php
	error_reporting(0);
	session_start();
	ob_start();

	$name =  $_SESSION["strName"];
	if ($name) {
		//echo "has login";


?>
<!DOCTYPE HTML>
<html>
<head>
<title>ระบบครุภัณฑ์อิเล็กทรอนิกส์</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet"> 
<script src="js/jquery.min.js"> </script>
<script src="js/Chart.js"></script>
</head>
<body>
<div id="wrapper">
<nav class="navbar navbar-default">
  <div class="container-fluid" >
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header" >
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
	  </button>
	  &nbsp;&nbsp;&nbsp;&nbsp;
      <a href="#"><img src="images/logo.png" width="150px" height="60px"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $name; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i>ออกจากระบบ</a></li>
            <!-- <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li> -->
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  
</nav>



		    <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
				
                    <li>
                        <a href="main_assets_list.php" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">รายการครุภัณฑ์ที่ถือครอง</span> </a>
                    </li>
                   
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">จัดการข้อมูล</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                        <li><a href="main_brands.php" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>ยี่ห้อ</a></li>
                         <li><a href="main_categories_type.php" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>ประเภท</a></li>
                         <li><a href="main_asset_used.php" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>สถานที่ตั้ง</a></li>
                            
                        <li><a href="main_categories.php" class=" hvr-bounce-to-right"><i class="fa fa-map-marker nav_icon"></i>หมวดหมู่</a></li>
			
						<li><a href="main_user.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>ผู้ใช้งาน</a></li>
                        <li><a href="main_status.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>สถานะ</a></li>
                        <li><a href="main_departments.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>หน่วยงาน</a></li>
                        <li><a href="main_prefixes.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>คำนำหน้า</a></li>
                        <li><a href="main_positions.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>ตำแหน่ง</a></li>
                        <li><a href="main_levels.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>ระดับ</a></li>
                        <li><a href="main_budgettypes.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>แหล่งงบประมาณ</a></li>
                        <li><a href="main_rcvtypes.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>วิธีการได้มา</a></li>
                        <li><a href="main_years.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>ปีงบประมาณ</a></li>
                        <li><a href="main_project.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>โครงการ</a></li>
                        <li><a href="main_locations.php" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>แหล่งที่มา</a></li>

                        
					   </ul>
                    </li>
					 
                     <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-desktop nav_icon"></i> <span class="nav-label">ดำเนินการ</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="AssetsNew.php" class=" hvr-bounce-to-right"> <i class="fa fa-info-circle nav_icon"></i>ขึ้นทะเบียนครุภัณฑ์ใหม่</a></li>
                            <li><a href="AssetsManage.php" class=" hvr-bounce-to-right"><i class="fa fa-question-circle nav_icon"></i>จัดการทะเบียนครุภัณฑ์</a></li>
                            <li><a href="AssetsStock.php" class=" hvr-bounce-to-right"><i class="fa fa-file-o nav_icon"></i>จำหน่าย/ตัดสต๊อกวาระ 8 ปี</a></li>
                            <li><a href="AssetsStockNon.php" class=" hvr-bounce-to-right"><i class="fa fa-file-o nav_icon"></i>จำหน่าย/ตัดสต๊อก กรณีอื่น</a></li>
                            
                            
                       </ul>
                    </li>
                     
                   
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label">รายงาน</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                       
                            <li><a href="ReportStock.php" class=" hvr-bounce-to-right"><i class="fa fa-check-square-o nav_icon"></i>รายงานจำหน่าย/ตัดสต๊อก</a></li>
                            <li><a href="ReportType.php" class=" hvr-bounce-to-right"><i class="fa fa-check-square-o nav_icon"></i>สรุปตามประเภทครุภัณฑ์</a></li>
                            <li><a href="Reportlist.php" class=" hvr-bounce-to-right"><i class="fa fa-check-square-o nav_icon"></i>รายการประเภทครุภัณฑ์</a></li>
                            <li><a href="ReportReciept.php" class=" hvr-bounce-to-right"><i class="fa fa-check-square-o nav_icon"></i>รายงานบัญชีรับ-จ่ายครุภัณฑ์</a></li>
                            <li><a href="ReportControl.php" class=" hvr-bounce-to-right"><i class="fa fa-check-square-o nav_icon"></i>รายงานทะเบียนคุมทรัพย์สิน</a></li>
                             <li><a href="ReportQR.php" class=" hvr-bounce-to-right"><i class="fa fa-check-square-o nav_icon"></i>รายงาน QR-CODE</a></li>
                        </ul>
                    </li>
                   
                    <li>
                        <a href="logout.php" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">ออกจากระบบ</span><span class="fa arrow"></span></a>

                    </li>
                </ul>
            </div>
			</div>
        </nav></div>
		<br><br><br>
		<div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">

 	<!--banner-->	
		   <div class="banner">
		    	<h2>
				<a href="main.php">หน้าหลัก</a>
				<i class="fa fa-angle-right"></i>
				<span>รายการครุภัณฑ์ที่ถือครอง</span>
				</h2>
		    </div>
		<!--//banner-->
	
 	<!--//grid-->
 	<div class="graph">
<div class="graph-grid">
  <div class="clearfix"> </div>
</div>
	<div class="col-md-12" style="width: 100%">	
							<div class="grid-1">
							 <?php
							 include 'assetsview_main.php';
							 ?>
							  <div class="clearfix"> </div>
						</div>
						
					</div>	
<!------------------------------------->
				
					<div class="graph-box">
					  <div class="col-md-4 graph-4 "></div>
					  <div class="clearfix"> </div>
					</div>				
				</div>
				
		<!---->
		<div class="copy">
            <p> &copy; สงวนลิขสิทธิ์ © 2560 กรมธุรกิจพลังงาน กระทรวงพลังงาน <a href="http://www.doeb.go.th/" target="_blank"></a> </p>    </div>
		</div>
		</div>
		<div class="clearfix"> </div>
       </div>
  
<!---->

<script src="js/bootstrap.min.js"> </script>
  
  
<!-- Mainly scripts -->
<script src="js/jquery.metisMenu.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<link href="css/custom.css" rel="stylesheet">
<script src="js/custom.js"></script>
<script src="js/screenfull.js"></script>
		<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});
			

			
		});
		</script>

<!----->

<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
</body>
</html>

<?php
}
	else{
		echo "don't login";
	}
?>