<?php
	error_reporting(0);
	ob_start();
	

?>
<?php
 function valid($name,$start_date,$end_date,$error)
 {
 ?>
<!DOCTYPE html>
<html>
    <script src="jquery-2.1.3.min.js"></script>

    <link href="bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="bootstrap-3.3.7-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <script src="bootstrap-3.3.7-dist/js/bootstrap.js"></script>

    <link href="dist/css/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="dist/js/bootstrap-datepicker-custom.js"></script>
    <script src="dist/locales/bootstrap-datepicker.th.min.js" charset="UTF-8"></script>

    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true              //Set เป็นปี พ.ศ.
            }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
        });

    </script>
<style>
input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    width: 25%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}




</style>
<body>
 <?php

 if ($error != '')
 {
 echo '<div style="padding:4px; border:1px solid red; color:red;">'.$error.'</div>';
 }
 
 $yeaID = $_GET['yeaID'];

include("config.php");
$result = mysql_query("SELECT * FROM years WHERE id=$yeaID")
 or die(mysql_error());
 $row = mysql_fetch_array($result);

 ?>

<div>
  <form action="years_edit_detail.php" method="post">
    <label for="year">ปีงบประมาณ</label>
    <input type="text" id="year" name="year" value="<?php echo $row["year"];?>">
    <label for="start_date">จากวันที่</label>
    <input type="text" id="start_date" name="start_date"  value="<?php echo $row["start_date"];?>" class="datepicker" data-date-format="mm/dd/yyyy">
    <label for="end_date">ถึงวันที่</label>
    <input type="text" id="end_date" name="end_date" value="<?php echo $row["end_date"];?>" class="datepicker" data-date-format="mm/dd/yyyy">
    <input type="hidden" id="yearid" name="yearid" value="<?php echo $row["id"];?>">

  <input type="submit" name="submit" value="แก้ไข">
  </form>
</div>

</body>
</html>
 <?php

}

include("config.php");

 if (isset($_POST['submit']))
 {

 $year = mysql_real_escape_string(htmlspecialchars($_POST['year']));
 $start_date = mysql_real_escape_string(htmlspecialchars($_POST['start_date']));
 $start_date_new = (explode("/",$start_date));
 $start_date_new1 = ($start_date_new[2]-543)."-".$start_date_new[1]."-".$start_date_new[0];

 $end_date = mysql_real_escape_string(htmlspecialchars($_POST['end_date']));
 $end_date_new = (explode("/",$end_date));
 $end_date_new1 = ($end_date_new[2]-543)."-".$end_date_new[1]."-".$end_date_new[0];
 $yearid = mysql_real_escape_string(htmlspecialchars($_POST['yearid']));
 
  

 if ($year == '' )
 {

 $error = 'Please enter the details!';

 valid($name,$start_date,$end_date,$error);
 }
 else
 {

 mysql_query("UPDATE years SET year='$year',start_date='$start_date_new1',end_date='$end_date_new1' WHERE id='$yearid'")
 or die(mysql_error());

 header("Location: main_years.php");

 }
 }
 else
 {
 valid('','','','');
 }
?>