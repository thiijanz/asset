<?php

/**
 * @author Ravi Tamada
 * @link http://www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/ Complete tutorial
 */

class DB_Functions {

    private $conn;

    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }

    // destructor
    function __destruct() {
        
    }

    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($name, $password) {
        
       
        $encrypted_password = $password; // encrypted password
        $salt = "DDD"; // salt

        $stmt = $this->conn->prepare("INSERT INTO userss(name, encrypted_password, salt) VALUES( ?, ?, ?)");
        $stmt->bind_param("sss",$name,  $encrypted_password, $salt);
        $result = $stmt->execute();
       

            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            return $user;
     
    }

    /**
     * Get user by email and password
     */

    /**
     * Check user is existed or not
     */


    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */


}

?>
