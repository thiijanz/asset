<?php
error_reporting(0);
ob_start();
?>
<!doctype html public "-//w3c//dtd html 3.2//en">

<html>
<script src="js/jquery-1.11.1.min.js"></script>
<head>
<title></title>

    <script type="text/javascript">
            $(document).ready(function() {
                $('#departments').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {departments: $(this).val()},
                        url: 'showUser.php',
                        success: function(data) {
                            $('#users').html(data);
                        }
                    });
                    return false;
                });
            });
        </script>
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 11px 28px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 4px 2px;
    cursor: pointer;
}
.button2 {border-radius: 4px;}

</style>
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}
input[type=text], select ,textarea {
    width: 100%;
	padding: 5px 1px;
    margin: 1px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=submit] {
  
    background-color: #337ab7;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}
th, td {
    border: none;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>

<body>

<form name="frmSearch" method="GET" action="<?php echo $_SERVER['SCRIPT_NAME'];?>">

<?php
include("config.php");
?>
  <table>
    <thead>
      <tr>
        <th>
<label class="radio-inline">
      <input type="radio" id="code2" value="code2" name="optradio" checked><b>เลขครุภัณฑ์</b>
    </label>
	<!-- <label class="radio-inline">
      <input type="radio" id="serial_no" value="serial_no" name="optradio">เลขครุภัณฑ์เดิม
    </label> -->
    <label class="radio-inline">
      <input type="radio" id="serial_no" value="serial_no" name="optradio">Serial No.
    </label>
	<?php
	   $strvalue = null; 
	?>
</th>	
<th>ปีงบประมาณ</th>
<th>สถานะ</th>
<th>ใช้ประจำที่</th>
<th colspan="2">ผู้ถือครอง</th>
</tr>
</thead>

<tr>
<td bgcolor="#f9f9f9"><input type="text" id="strnumber" name="strnumber" value="<?php echo $strvalue;?>" placeholder="เลขครุภัณฑ์ หรือ Serial No."></td>
<td bgcolor="#f9f9f9"><select name="year">
            <option value="">เลือกปีงบประมาณ</option>
			<?php
			$strSQL1 = "SELECT * FROM years ORDER BY year DESC";
			$objQuery1 = mysql_query($strSQL1);
			while($objResuut1 = mysql_fetch_array($objQuery1))
			{
			?>
			<option value="<?php echo $objResuut1["id"];?>"><?php echo $objResuut1["year"];?></option>
			<?php
			}
			?>
		  </select></td>
<td bgcolor="#f9f9f9"><select name="statuses">
            <option value="">เลือกสถานะ</option>
			<?php
			$strSQL9 = "SELECT * FROM statuses ORDER BY id ASC";
			$objQuery9 = mysql_query($strSQL9);
			while($objResuut9 = mysql_fetch_array($objQuery9))
			{
			?>
			<option value="<?php echo $objResuut9["id"];?>"><?php echo $objResuut9["description"];?></option>
			<?php
			}
			?>
		  </select></td>
<td bgcolor="#f9f9f9"><select name="departments" id="departments">
            <option value="">เลือกที่ตั้ง</option>
			<?php
			$strSQL5 = "SELECT * FROM departments ORDER BY id ASC";
			$objQuery5 = mysql_query($strSQL5);
			while($objResuut5 = mysql_fetch_array($objQuery5))
			{
			?>
			<option value="<?php echo $objResuut5["id"];?>"><?php echo $objResuut5["abbr"];?></option>
			<?php
			}
			?>
		  </select></td>
<td bgcolor="#f9f9f9"><select name="users" id="users">
<option value="">เลือกผู้ถือครอง</option>
		  </select></td>

		  <td bgcolor="#f9f9f9"><input type="submit" value="ค้นหา"></td>
</tr>


</table> 

</form>

<?php
$i=1;
if (($_GET["year"] != "") && ($_GET["departments"] != "") && ($_GET["statuses"] != "") )
	{
	
	$strSQL = "SELECT * FROM assets WHERE year_id ='".$_GET["year"]."' and status_id ='".$_GET["statuses"]."' and location_dept_id ='".$_GET["departments"]."' and alive_flag <>2";
	$objQuery = mysql_query($strSQL)or die ("Error Query [".$strSQL."]");
	$Num_Rows = mysql_num_rows($objQuery);
	//}



	$Per_Page = 30;   // Per Page

	$Page = $_GET["Page"];
	if(!$_GET["Page"])
	{
		$Page=1;
	}

	$Prev_Page = $Page-1;
	$Next_Page = $Page+1;

	$Page_Start = (($Per_Page*$Page)-$Per_Page);
	if($Num_Rows<=$Per_Page)
	{
		$Num_Pages =1;
	}
	else if(($Num_Rows % $Per_Page)==0)
	{
		$Num_Pages =($Num_Rows/$Per_Page) ;
	}
	else
	{
		$Num_Pages =($Num_Rows/$Per_Page)+1;
		$Num_Pages = (int)$Num_Pages;
	}
	$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
	$objQuery  = mysql_query($strSQL);
?>

  <table>
    <tr>
      <th width="6%"><font color="#0099FF">#</font></th>
      <th width="12%"><font color="#0099FF">เลขทะเบียน</font></th>
      <th width="27%"><font color="#0099FF">ยี่ห้อ ชนิด แบบ ขนาดและลักษณะ</font></th>
      <th width="15%"><font color="#0099FF">รหัส GFMIS</font></th>
      <th width="14%"><font color="#0099FF">ราคาต่อหน่วย</font></th>
      <th width="13%"><font color="#0099FF">ใช้ประจำที่</font></th>
      
    </tr>
    <?php
	$num1 = 0;

while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
      <td><?=(($Per_Page*$Page)+$i)?></td>
      <td><a href="main_assets_tranfer_view.php?assID=<?php echo $objResult["id"];?>"><?php echo $objResult["code2"];?></a></td>
      <td><?php echo $objResult["detail"];?></td>
      <td><?php echo $objResult["gfmiscode"];?></td>
      <td><?php echo number_format($objResult["unitprice"], 2);?></td>
      <td><?php 
	  
	  //echo $objResult["location_dept_id"];
	  $location_dept_id = $objResult["location_dept_id"];
$result_lo = mysql_query("SELECT * FROM departments WHERE id='$location_dept_id'")
 or die(mysql_error());
$row_lo = mysql_fetch_array($result_lo);
echo $row_lo["abbr"];
	  
	  ?>
      

    </tr>
  <?php
   $i++;
	}
  ?>

  </table>
</div>


Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page : 
	<?php
	if($Prev_Page)
	{
		echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page&year=$_GET[year]&statuses=$_GET[statuses]&departments=$_GET[departments]'><< Back</a> ";
	}

	for($i=1; $i<=$Num_Pages; $i++){
		if($i != $Page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i&year=$_GET[year]&statuses=$_GET[statuses]&departments=$_GET[departments]'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
	if($Page!=$Num_Pages)
	{
		echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page&year=$_GET[year]&statuses=$_GET[statuses]&departments=$_GET[departments]'>Next>></a> ";
	}

	}

else if($_GET["year"] != "")
{
	include("config.php");
$years = $_GET["years"];
$strSQL = "SELECT * FROM assets WHERE (year_id ='".$_GET["year"]."') and alive_flag <>2";
//}

$objQuery = mysql_query($strSQL)or die ("Error Query [".$strSQL."]");
	$Num_Rows = mysql_num_rows($objQuery);


	$Per_Page = 30;   // Per Page

	$Page = $_GET["Page"];
	if(!$_GET["Page"])
	{
		$Page=1;
	}

	$Prev_Page = $Page-1;
	$Next_Page = $Page+1;

	$Page_Start = (($Per_Page*$Page)-$Per_Page);
	if($Num_Rows<=$Per_Page)
	{
		$Num_Pages =1;
	}
	else if(($Num_Rows % $Per_Page)==0)
	{
		$Num_Pages =($Num_Rows/$Per_Page) ;
	}
	else
	{
		$Num_Pages =($Num_Rows/$Per_Page)+1;
		$Num_Pages = (int)$Num_Pages;
	}
	$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
	$objQuery  = mysql_query($strSQL);
?>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table> <br>
  <table>
    <tr>
      <th width="6%"><font color="#0099FF">#</font></th>
      <th width="12%"><font color="#0099FF">เลขทะเบียน</font></th>
      <th width="27%"><font color="#0099FF">ยี่ห้อ ชนิด แบบ ขนาดและลักษณะ</font></th>
      <th width="15%"><font color="#0099FF">รหัส GFMIS</font></th>
      <th width="14%"><font color="#0099FF">ราคาต่อหน่วย</font></th>
      <th width="13%"><font color="#0099FF">ใช้ประจำที่</font></th>
      
    </tr>
    <?php
	$num1 = 0;
while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
      <td><?php echo $num1;?></td>
      <td><a href="main_assets_tranfer_view.php?assID=<?php echo $objResult["id"];?>"><?php echo $objResult["code2"];?></a></td>
      <td><?php echo $objResult["detail"];?></td>
      <td><?php echo $objResult["gfmiscode"];?></td>
      <td><?php echo number_format($objResult["unitprice"], 2);?></td>
      <td><?php 
	  
	  //echo $objResult["location_dept_id"];
	  $location_dept_id = $objResult["location_dept_id"];
$result_lo = mysql_query("SELECT * FROM departments WHERE id='$location_dept_id'")
 or die(mysql_error());
$row_lo = mysql_fetch_array($result_lo);
echo $row_lo["abbr"];
	  
	  ?>
      

    </tr>
  <?php
	}
  ?>

  </table>
</div>


Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page :
	<?php
	if($Prev_Page)
	{
		echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page&year=$_GET[year]'><< Back</a> ";
	}

	for($i=1; $i<=$Num_Pages; $i++){
		if($i != $Page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i&year=$_GET[year]'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
	if($Page!=$Num_Pages)
	{
		echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page&year=$_GET[year]'>Next>></a> ";
	}
		//mysql_close($objConnect);

	}
	else if($_GET["statuses"] != "")
	{
	$strSQL = "SELECT * FROM assets WHERE (status_id ='".$_GET["statuses"]."') and alive_flag <>2";
$objQuery = mysql_query($strSQL)or die ("Error Query [".$strSQL."]");
	$Num_Rows = mysql_num_rows($objQuery);
	//}
	
	$Per_Page = 30;   // Per Page

	$Page = $_GET["Page"];
	if(!$_GET["Page"])
	{
		$Page=1;
	}

	$Prev_Page = $Page-1;
	$Next_Page = $Page+1;

	$Page_Start = (($Per_Page*$Page)-$Per_Page);
	if($Num_Rows<=$Per_Page)
	{
		$Num_Pages =1;
	}
	else if(($Num_Rows % $Per_Page)==0)
	{
		$Num_Pages =($Num_Rows/$Per_Page) ;
	}
	else
	{
		$Num_Pages =($Num_Rows/$Per_Page)+1;
		$Num_Pages = (int)$Num_Pages;
	}
	$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
	$objQuery  = mysql_query($strSQL);
?>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table> <br>
  <table>
    <tr>
      <th width="6%"><font color="#0099FF">#</font></th>
      <th width="12%"><font color="#0099FF">เลขทะเบียน</font></th>
      <th width="27%"><font color="#0099FF">ยี่ห้อ ชนิด แบบ ขนาดและลักษณะ</font></th>
      <th width="15%"><font color="#0099FF">รหัส GFMIS</font></th>
      <th width="14%"><font color="#0099FF">ราคาต่อหน่วย</font></th>
      <th width="13%"><font color="#0099FF">ใช้ประจำที่</font></th>
    
    </tr>
    <?php
	$num1 = 0;
while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
      <td><?php echo $num1;?></td>
      <td><a href="main_assets_tranfer_view.php?assID=<?php echo $objResult["id"];?>"><?php echo $objResult["code2"];?></a></td>
      <td><?php echo $objResult["detail"];?></td>
      <td><?php echo $objResult["gfmiscode"];?></td>
      <td><?php echo number_format($objResult["unitprice"], 2);?></td>
      <td><?php 
	  
	  //echo $objResult["location_dept_id"];
	  $location_dept_id = $objResult["location_dept_id"];
$result_lo = mysql_query("SELECT * FROM departments WHERE id='$location_dept_id'")
 or die(mysql_error());
$row_lo = mysql_fetch_array($result_lo);
echo $row_lo["abbr"];
	  
	  ?>
      

    </tr>
  <?php
	}
  ?>

  </table>
</div>


Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page :
	<?php
	if($Prev_Page)
	{
		echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page&statuses=$_GET[statuses]'><< Back</a> ";
	}

	for($i=1; $i<=$Num_Pages; $i++){
		if($i != $Page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i&statuses=$_GET[statuses]'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
	if($Page!=$Num_Pages)
	{
		echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page&statuses=$_GET[statuses]'>Next>></a> ";
	}
		//mysql_close($objConnect);
   }
	  else if($_GET["departments"] != "")
	{

	$strSQL = "SELECT * FROM assets WHERE (location_dept_id ='".$_GET["departments"]."') and alive_flag <>2"; 
	$objQuery = mysql_query($strSQL)or die ("Error Query [".$strSQL."]");
	$Num_Rows = mysql_num_rows($objQuery);
	//}
	
	$Per_Page = 30;   // Per Page

	$Page = $_GET["Page"];
	if(!$_GET["Page"])
	{
		$Page=1;
	}

	$Prev_Page = $Page-1;
	$Next_Page = $Page+1;

	$Page_Start = (($Per_Page*$Page)-$Per_Page);
	if($Num_Rows<=$Per_Page)
	{
		$Num_Pages =1;
	}
	else if(($Num_Rows % $Per_Page)==0)
	{
		$Num_Pages =($Num_Rows/$Per_Page) ;
	}
	else
	{
		$Num_Pages =($Num_Rows/$Per_Page)+1;
		$Num_Pages = (int)$Num_Pages;
	}
	$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
	$objQuery  = mysql_query($strSQL);
?>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table> <br>
  <table>
    <tr>
      <th width="6%"><font color="#0099FF">#</font></th>
      <th width="12%"><font color="#0099FF">เลขทะเบียน</font></th>
      <th width="27%"><font color="#0099FF">ยี่ห้อ ชนิด แบบ ขนาดและลักษณะ</font></th>
      <th width="15%"><font color="#0099FF">รหัส GFMIS</font></th>
      <th width="14%"><font color="#0099FF">ราคาต่อหน่วย</font></th>
      <th width="13%"><font color="#0099FF">ใช้ประจำที่</font></th>
      
    </tr>
    <?php
	$num1 = 0;
while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
      <td><?php //echo $num1;?><?php echo (($Per_Page*($Page-1))+$i);?><?php //echo ($Page == 1 ? $i : ($Per_Page*$Page==60 ? ((60/2)+$i) : (($Per_Page*($Page-1))+$i) ) ); ?></td>
      <td><a href="main_assets_tranfer_view.php?assID=<?php echo $objResult["id"];?>"><?php echo $objResult["code2"];?></a></td>
      <td><?php echo $objResult["detail"];?></td>
      <td><?php echo $objResult["gfmiscode"];?></td>
      <td><?php echo number_format($objResult["unitprice"], 2);?></td>
      <td><?php 
	  
	  //echo $objResult["location_dept_id"];
	  $location_dept_id = $objResult["location_dept_id"];
$result_lo = mysql_query("SELECT * FROM departments WHERE id='$location_dept_id'")
 or die(mysql_error());
$row_lo = mysql_fetch_array($result_lo);
echo $row_lo["abbr"];
	  
	  ?>
      

    </tr>
  <?php
	$i++;
	}
  ?>

  </table>
</div>


Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page :
	<?php
	if($Prev_Page)
	{
		echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page&departments=$_GET[departments]'><< Back</a> ";
	}

	for($i=1; $i<=$Num_Pages; $i++){
		if($i != $Page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i&departments=$_GET[departments]'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
	if($Page!=$Num_Pages)
	{
		echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page&departments=$_GET[departments]'>Next>></a> ";
	}

	}
	
	else if($_GET["users"] != "")
		{
		$strSQL = "SELECT * FROM assets WHERE (owner_user_id ='".$_GET["users"]."') and alive_flag <>2";
	$objQuery = mysql_query($strSQL)or die ("Error Query [".$strSQL."]");
	$Num_Rows = mysql_num_rows($objQuery);
	//}
	
	$Per_Page = 30;   // Per Page

	$Page = $_GET["Page"];
	if(!$_GET["Page"])
	{
		$Page=1;
	}

	$Prev_Page = $Page-1;
	$Next_Page = $Page+1;

	$Page_Start = (($Per_Page*$Page)-$Per_Page);
	if($Num_Rows<=$Per_Page)
	{
		$Num_Pages =1;
	}
	else if(($Num_Rows % $Per_Page)==0)
	{
		$Num_Pages =($Num_Rows/$Per_Page) ;
	}
	else
	{
		$Num_Pages =($Num_Rows/$Per_Page)+1;
		$Num_Pages = (int)$Num_Pages;
	}
	$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
	$objQuery  = mysql_query($strSQL);
?>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table> <br>
  <table>
    <tr>
      <th width="6%"><font color="#0099FF">#</font></th>
      <th width="12%"><font color="#0099FF">เลขทะเบียน</font></th>
      <th width="27%"><font color="#0099FF">ยี่ห้อ ชนิด แบบ ขนาดและลักษณะ</font></th>
      <th width="15%"><font color="#0099FF">รหัส GFMIS</font></th>
      <th width="14%"><font color="#0099FF">ราคาต่อหน่วย</font></th>
      <th width="13%"><font color="#0099FF">ใช้ประจำที่</font></th>
     
    </tr>
    <?php
	$num1 = 0;
while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
      <td><?php echo $num1;?></td>
      <td><a href="main_assets_tranfer_view.php?assID=<?php echo $objResult["id"];?>"><?php echo $objResult["code2"];?></a></td>
      <td><?php echo $objResult["detail"];?></td>
      <td><?php echo $objResult["gfmiscode"];?></td>
      <td><?php echo number_format($objResult["unitprice"], 2);?></td>
      <td><?php 
	  
	  //echo $objResult["location_dept_id"];
	  $location_dept_id = $objResult["location_dept_id"];
$result_lo = mysql_query("SELECT * FROM departments WHERE id='$location_dept_id'")
 or die(mysql_error());
$row_lo = mysql_fetch_array($result_lo);
echo $row_lo["abbr"];
	  
	  ?>
      

    </tr>
  <?php
	}
  ?>

  </table>
</div>


Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page :
	<?php
	if($Prev_Page)
	{
		echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page&users=$_GET[users]'><< Back</a> ";
	}

	for($i=1; $i<=$Num_Pages; $i++){
		if($i != $Page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i&users=$_GET[users]'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
	if($Page!=$Num_Pages)
	{
		echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page&users=$_GET[users]'>Next>></a> ";
	}

	}
	else if($_GET["optradio"] == "code2")
	{
	$strSQL = "SELECT * FROM assets WHERE code2 ='".$_GET["strnumber"]."' and alive_flag <>2 ";
	$objQuery = mysql_query($strSQL)or die ("Error Query [".$strSQL."]");
	$Num_Rows = mysql_num_rows($objQuery);
	//}
	
	$Per_Page = 30;   // Per Page

	$Page = $_GET["Page"];
	if(!$_GET["Page"])
	{
		$Page=1;
	}

	$Prev_Page = $Page-1;
	$Next_Page = $Page+1;

	$Page_Start = (($Per_Page*$Page)-$Per_Page);
	if($Num_Rows<=$Per_Page)
	{
		$Num_Pages =1;
	}
	else if(($Num_Rows % $Per_Page)==0)
	{
		$Num_Pages =($Num_Rows/$Per_Page) ;
	}
	else
	{
		$Num_Pages =($Num_Rows/$Per_Page)+1;
		$Num_Pages = (int)$Num_Pages;
	}
	$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
	$objQuery  = mysql_query($strSQL);
?>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table> <br>
  <table>
    <tr>
      <th width="6%"><font color="#0099FF">#</font></th>
      <th width="12%"><font color="#0099FF">เลขทะเบียน</font></th>
      <th width="27%"><font color="#0099FF">ยี่ห้อ ชนิด แบบ ขนาดและลักษณะ</font></th>
      <th width="15%"><font color="#0099FF">รหัส GFMIS</font></th>
      <th width="14%"><font color="#0099FF">ราคาต่อหน่วย</font></th>
      <th width="13%"><font color="#0099FF">ใช้ประจำที่</font></th>
    
    </tr>
    <?php
	$num1 = 0;
while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
      <td><?php echo $num1;?></td>
      <td><a href="main_assets_tranfer_view.php?assID=<?php echo $objResult["id"];?>"><?php echo $objResult["code2"];?></a></td>
      <td><?php echo $objResult["detail"];?></td>
      <td><?php echo $objResult["gfmiscode"];?></td>
      <td><?php echo number_format($objResult["unitprice"], 2);?></td>
      <td><?php 
	  
	  //echo $objResult["location_dept_id"];
	  $location_dept_id = $objResult["location_dept_id"];
$result_lo = mysql_query("SELECT * FROM departments WHERE id='$location_dept_id'")
 or die(mysql_error());
$row_lo = mysql_fetch_array($result_lo);
echo $row_lo["abbr"];
	  
	  ?>
      

    </tr>
  <?php
	}
  ?>

  </table>
</div>

Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page :
	<?php
	if($Prev_Page)
	{
		echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page&users=$_GET[users]'><< Back</a> ";
	}

	for($i=1; $i<=$Num_Pages; $i++){
		if($i != $Page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i&users=$_GET[users]'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
	if($Page!=$Num_Pages)
	{
		echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page&users=$_GET[users]'>Next>></a> ";
	}

	}
	else if($_GET["optradio"] == "serial_no")
	{
	$strSQL = "SELECT * FROM assets WHERE serial_no ='".$_GET["strnumber"]."' and alive_flag <>2 ";
	$objQuery = mysql_query($strSQL)or die ("Error Query [".$strSQL."]");
	$Num_Rows = mysql_num_rows($objQuery);
	//}
	
	$Per_Page = 30;   // Per Page

	$Page = $_GET["Page"];
	if(!$_GET["Page"])
	{
		$Page=1;
	}

	$Prev_Page = $Page-1;
	$Next_Page = $Page+1;

	$Page_Start = (($Per_Page*$Page)-$Per_Page);
	if($Num_Rows<=$Per_Page)
	{
		$Num_Pages =1;
	}
	else if(($Num_Rows % $Per_Page)==0)
	{
		$Num_Pages =($Num_Rows/$Per_Page) ;
	}
	else
	{
		$Num_Pages =($Num_Rows/$Per_Page)+1;
		$Num_Pages = (int)$Num_Pages;
	}
	$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
	$objQuery  = mysql_query($strSQL);
?>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table> <br>
  <table>
    <tr>
      <th width="6%"><font color="#0099FF">#</font></th>
      <th width="12%"><font color="#0099FF">เลขทะเบียน</font></th>
      <th width="27%"><font color="#0099FF">ยี่ห้อ ชนิด แบบ ขนาดและลักษณะ</font></th>
      <th width="15%"><font color="#0099FF">รหัส GFMIS</font></th>
      <th width="14%"><font color="#0099FF">ราคาต่อหน่วย</font></th>
      <th width="13%"><font color="#0099FF">ใช้ประจำที่</font></th>
    
    </tr>
    <?php
	$num1 = 0;
while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
      <td><?php echo $num1;?></td>
      <td><a href="main_assets_tranfer_view.php?assID=<?php echo $objResult["id"];?>"><?php echo $objResult["code2"];?></a></td>
      <td><?php echo $objResult["detail"];?></td>
      <td><?php echo $objResult["gfmiscode"];?></td>
      <td><?php echo number_format($objResult["unitprice"], 2);?></td>
      <td><?php 
	  
	  //echo $objResult["location_dept_id"];
	  $location_dept_id = $objResult["location_dept_id"];
$result_lo = mysql_query("SELECT * FROM departments WHERE id='$location_dept_id'")
 or die(mysql_error());
$row_lo = mysql_fetch_array($result_lo);
echo $row_lo["abbr"];
	  
	  ?>
      

    </tr>
  <?php
	}
  ?>

  </table>
</div>

Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page :
	<?php
	if($Prev_Page)
	{
		echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page&code1=$_GET[code1]'><< Back</a> ";
	}

	for($i=1; $i<=$Num_Pages; $i++){
		if($i != $Page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i&code1=$_GET[code1]'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
	if($Page!=$Num_Pages)
	{
		echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page&code1=$_GET[code1]'>Next>></a> ";
	}

	}

    else
{
	$strSQL = "SELECT * FROM assets WHERE alive_flag <>2";
		$objQuery = mysql_query($strSQL)or die ("Error Query [".$strSQL."]");
	$Num_Rows = mysql_num_rows($objQuery);
	//}
	
	$Per_Page = 30;   // Per Page

	$Page = $_GET["Page"];
	if(!$_GET["Page"])
	{
		$Page=1;
	}

	$Prev_Page = $Page-1;
	$Next_Page = $Page+1;

	$Page_Start = (($Per_Page*$Page)-$Per_Page);
	if($Num_Rows<=$Per_Page)
	{
		$Num_Pages =1;
	}
	else if(($Num_Rows % $Per_Page)==0)
	{
		$Num_Pages =($Num_Rows/$Per_Page) ;
	}
	else
	{
		$Num_Pages =($Num_Rows/$Per_Page)+1;
		$Num_Pages = (int)$Num_Pages;
	}
	$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
	$objQuery  = mysql_query($strSQL);
?>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table> <br>

  <table>
    <tr>
	
      <th width="6%"><font color="#0099FF">#</font></th>
      <th width="12%"><font color="#0099FF">เลขทะเบียน</font></th>
      <th width="27%"><font color="#0099FF">ยี่ห้อ ชนิด แบบ ขนาดและลักษณะ</font></th>
      <th width="15%"><font color="#0099FF">รหัส GFMIS</font></th>
      <th width="14%"><font color="#0099FF">ราคาต่อหน่วย</font></th>
      <th width="13%"><font color="#0099FF">ใช้ประจำที่</font></th>
      
    </tr>
    <?php
	$num1 = 0;
while($objResult = mysql_fetch_array($objQuery))
{
	$num1 = $num1+1
?>
    <tr>
	
      <td><?php echo $num1;?></td>
      <td><a href="main_assets_tranfer_view.php?assID=<?php echo $objResult["id"];?>"><?php echo $objResult["code2"];?></a></td>
      <td><?php echo $objResult["detail"];?></td>
      <td><?php echo $objResult["gfmiscode"];?></td>
      <td><?php echo number_format($objResult["unitprice"], 2);?></td>
      <td><?php 
	  
	  //echo $objResult["location_dept_id"];
	  $location_dept_id = $objResult["location_dept_id"];
$result_lo = mysql_query("SELECT * FROM departments WHERE id='$location_dept_id'")
 or die(mysql_error());
$row_lo = mysql_fetch_array($result_lo);
echo $row_lo["abbr"];
	  
	  ?></td>
     
      

    </tr>
  <?php
	}
  ?>

  </table>
</div>


Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page :
	<?php
	if($Prev_Page)
	{
		echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page'><< Back</a> ";
	}

	for($i=1; $i<=$Num_Pages; $i++){
		if($i != $Page)
		{
			echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i'>$i</a> ]";
		}
		else
		{
			echo "<b> $i </b>";
		}
	}
	if($Page!=$Num_Pages)
	{
		echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page'>Next>></a> ";
	}

	}

		
	?>
<?php 


?>
</body>

</html>
