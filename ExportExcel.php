<?php

error_reporting(E_ALL);

date_default_timezone_set('Asia/Bangkok');

/** PHPExcel */
require_once 'Classes/PHPExcel.php';


// Create new PHPExcel object

$objPHPExcel = new PHPExcel();

// Set properties

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


// Add some data

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'เลขครุภัณฑ์')
            ->setCellValue('B1', 'รายการ')
            ->setCellValue('C1', 'gfmis')
			->setCellValue('D1', 'ราคา')
			->setCellValue('E1', 'หน่วย')
            ->setCellValue('F1', 'สถานที่ใช้');

// Write data from MySQL result
include("config.php");
$strSQL = "SELECT * FROM assets";
$objQuery = mysql_query($strSQL);
$i = 2;
while($objResult = mysql_fetch_array($objQuery))
{
	$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $objResult["code2"]);
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $objResult["detail"]);
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $objResult["gfmiscode"]);
	$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $objResult["unitprice"]);
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $objResult["unit_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $objResult["location_dept_id"]);
	$i++;
}
mysql_close($objConnect);

// Rename sheet

$objPHPExcel->getActiveSheet()->setTitle('รายการครุภัณฑ์');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Save Excel 2007 file

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$strFileName = "myData.xls";
$objWriter->save($strFileName);


// Echo memory peak usage


// Echo done

?>