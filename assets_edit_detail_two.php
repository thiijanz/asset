<?php
	error_reporting(0);
	ob_start();
	date_default_timezone_set('Asia/Bangkok');
	include("config.php");

?>
<?php
 function valid($code4,$code5,$code1,$asset_des,$model,$gfmiscode,$unitprice,$unit_name,$lifeperiod,$asset_income,$rcvtypes,$budgettypes,$desp,$asset_location,$statuses,$start_date_new1,$cate_id,$brands,$serial_no,$asset_remark,$departmentsold,$year_id,$asset_relation,$file,$asset_sup,$projects,$catedate,$catedate,$name,$error)
 {
 ?>
<!DOCTYPE html>
<html>
    <script src="jquery-2.1.3.min.js"></script>

    <link href="bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="bootstrap-3.3.7-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <script src="bootstrap-3.3.7-dist/js/bootstrap.js"></script>

    <link href="dist/css/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="dist/js/bootstrap-datepicker-custom.js"></script>
    <script src="dist/locales/bootstrap-datepicker.th.min.js" charset="UTF-8"></script>

    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true              //Set เป็นปี พ.ศ.
            }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
        });

    </script>
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    border: none;
    text-align: left;
    padding: 8px;
}

input[type=text], select ,textarea {
    width: 100%;
	padding: 5px 1px;
    margin: 1px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=submit] {
  
    background-color: #337ab7;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}
input[type=reset] {
   
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

</style>
<body>
 <?php

 if ($error != '')
 {
 echo '<div style="padding:4px; border:1px solid red; color:red;">'.$error.'</div>';
 }
$asssID = $_GET['asssID'];
include("config.php");
$result = mysql_query("SELECT * FROM assetshistories WHERE id=$asssID")
 or die(mysql_error());
$row = mysql_fetch_array($result);
 ?>


  <form action="assets_edit_detail.php" method="post">
  <div style="overflow-x:auto;">
    <table width="100%">
     <tr>
     <td width="17%" align="right" bgcolor="#d7ecf6">
     <label for="code" >หมวดหมู่ &nbsp;</label>
     </td>
     <td width="40%" bgcolor="#d7ecf6">
            <select name="code" >
			<?php
			$strSQL = "SELECT * FROM categories ORDER BY id ASC";
			$objQuery = mysql_query($strSQL);
			while($objResuut = mysql_fetch_array($objQuery))
			{
			?>
			<option value="<?php echo $objResuut["code"];?>"><?php echo $objResuut["code"];?> <?php echo $objResuut["description"];?></option>
			<?php
			}
			?>
		  </select>
     </td>
     <td  align="right" bgcolor="#d7ecf6">
     <label for="rcvdate">วันเริ่มต้น &nbsp;</label>
     </td>
     <td bgcolor="#d7ecf6">
     <input type="text" id="begindate" name="begindate" value=""  class="datepicker" data-date-format="mm/dd/yyyy" size="14">
     </td>
     </tr>
     </table>
     <table width="93%">
     <tr>
     <td width="16%" align="right" bgcolor="#d7ecf6">
     <label for="code1">เลขครุภัณฑ์ลำดับที่</label>
     </td>
     <td width="41%" bgcolor="#d7ecf6" >
     <input type="text" id="code1" name="code1" size="50" value="<?php echo $row["seq_no"];?>">
     <input type="hidden" id="asseid" name="asseid" value="<?php echo $row["id"];?>">
     </td>
     <td align="right" bgcolor="#d7ecf6">
     <label for="rcvdate">วันที่ได้มา &nbsp;</label>
     </td>
     <td bgcolor="#d7ecf6">
     <input type="text" id="rcvdate" name="rcvdate" value=""  class="datepicker" data-date-format="mm/dd/yyyy" size="20">
     </td>
     </tr>
     <tr>
     <td align="right" bgcolor="#d7ecf6">
      <label for="gfmiscode">รหัส GFMIS &nbsp;</label>
     </td>
     <td  bgcolor="#d7ecf6">
      <input type="text" id="gfmiscode" name="gfmiscode" size="50" value="<?php echo $row["gfmiscode"];?>">
     </td>
     <td align="right" bgcolor="#d7ecf6">
     <label for="years">ปีงบประมาณ &nbsp;</label>
     </td>
     <td bgcolor="#d7ecf6">
            <select name="years" >
			<?php
			$strSQL1 = "SELECT * FROM years ORDER BY id ASC";
			$objQuery1 = mysql_query($strSQL1);
			while($objResuut1 = mysql_fetch_array($objQuery1))
			{
			?>
			<option value="<?php echo $objResuut1["year"];?>"><?php echo $objResuut1["year"];?></option>
			<?php
			}
			?>
		  </select>
     </td>
     </tr>

      </table>
      <table width="100%">
     <tr>
     <td width="165" bgcolor="#fcf8e3"><label for="unitprice">ราคาต่อหน่วย(บาท)</label></td>
     <td width="206" bgcolor="#fcf8e3"><label for="lifeperiod">อายุการใช้งาน(ปี)</label></td>
     <td width="192" bgcolor="#fcf8e3"><label for="budgettypes">แหล่งงบ</label></td>
     <td width="530" bgcolor="#fcf8e3"><label for="asset_sup">แหล่งที่มา(Supplier)</label></td>
     <td width="530" bgcolor="#fcf8e3"><label for="rcvtypes">วิธีการได้มา</label></td>
     
     </tr>
     <tr>
     <td bgcolor="#f9f9f9"><input type="text" id="unitprice" name="unitprice" size="30" value="<?php echo $row["unitprice"];?>"></td>
     <td bgcolor="#f9f9f9"><input type="text" id="lifeperiod" name="lifeperiod" size="30" value="<?php echo $row["lifeperiod"];?>"></td>
     <td bgcolor="#f9f9f9"><select name="budgettypes" >
			<?php
			$strSQL2 = "SELECT * FROM budgettypes ORDER BY id ASC";
			$objQuery2 = mysql_query($strSQL2);
			while($objResuut2 = mysql_fetch_array($objQuery2))
			{
			?>
			<option value="<?php echo $objResuut2["id"];?>"><?php echo $objResuut2["description"];?></option>
			<?php
			}
			?>
		  </select></td>
      <td bgcolor="#f9f9f9"><select name="asset_sup" >
			<?php
			$strSQL11 = "SELECT * FROM locations ORDER BY id ASC";
			$objQuery11 = mysql_query($strSQL11);
			while($objResuut11 = mysql_fetch_array($objQuery11))
			{
			?>
			<option value="<?php echo $objResuut11["id"];?>"><?php echo $objResuut11["description"];?></option>
			<?php
			}
			?>
		  </select></td>
          <td bgcolor="#f9f9f9">
          <select name="rcvtypes" >
			<?php
			$strSQL3 = "SELECT * FROM rcvtypes ORDER BY id ASC";
			$objQuery3 = mysql_query($strSQL3);
			while($objResuut3 = mysql_fetch_array($objQuery3))
			{
			?>
			<option value="<?php echo $objResuut3["id"];?>"><?php echo $objResuut3["description"];?></option>
			<?php
			}
			?>
		  </select>
          </td>
     </tr>
    </table>
      <table width="100%">
      <tr>
     <td width="177" bgcolor="#d9edf7"><label for="brands">ยี่ห้อ</label></td>
     <td width="213" bgcolor="#d9edf7"><label for="model">รุ่น/แบบ(Model)</label></td>
     <td width="204" bgcolor="#d9edf7"><label for="serial_no">เลขประจำตัว(Serial no.)</label></td>
     <td width="222" bgcolor="#d9edf7"><label for="unit_name">หน่วย</label></td>
     <td width="222" bgcolor="#d9edf7"><label for="file">รูปครุภัณฑ์</label></td> 
      </tr>
      <tr>
     <td width="177" bgcolor="#f9f9f9"><select name="brands">
			<?php
			$strSQL4 = "SELECT * FROM brands ORDER BY id ASC";
			$objQuery4 = mysql_query($strSQL4);
			while($objResuut4 = mysql_fetch_array($objQuery4))
			{
			?>
			<option value="<?php echo $objResuut4["id"];?>"><?php echo $objResuut4["description"];?></option>
			<?php
			}
			?>
		  </select></td>
     <td width="213" bgcolor="#f9f9f9"><input type="text" id="model" name="model" value="<?php echo $row["model"];?>"></td>
     <td width="204" bgcolor="#f9f9f9"><input type="text" id="serial_no" name="serial_no" value="<?php echo $row["serial_no"];?>"></td>
     <td width="222" bgcolor="#f9f9f9"><input type="text" id="unit_name" name="unit_name" value="<?php echo $row["unit_name"];?>"></td>
     <td width="256" bgcolor="#f9f9f9"><input type="file" name="photo" id="photo"/></td>
      </tr>
    </table>
      <table>
      <tr>
     <td width="131" bgcolor="#f2dede"><label for="departments">ใช้ประจำที่สำนัก</label></td>
     <td width="136" bgcolor="#f2dede"><label for="departmentsold">สำนัก(ผู้ถือครอง)</label></td>
     <td width="174" bgcolor="#f2dede"><label for="users">ชื่อ-สกุล(ผู้ถือครอง)</label></td>
     <td width="299" bgcolor="#f2dede"><label for="projects">รหัสโครงการ</label></td>
     <td width="110" bgcolor="#f2dede"><label for="statuses">สถานะ</label></td>
     <td width="218" bgcolor="#f2dede"><label for="code4">ทดแทนครุภัณฑ์เลขที่</label></td>
      </tr>
      <tr>
     <td width="131" bgcolor="#f9f9f9"> <select name="departments" >
			<?php
			$strSQL5 = "SELECT * FROM departments ORDER BY id ASC";
			$objQuery5 = mysql_query($strSQL5);
			while($objResuut5 = mysql_fetch_array($objQuery5))
			{
			?>
			<option value="<?php echo $objResuut5["abbr"];?>"><?php echo $objResuut5["abbr"];?></option>
			<?php
			}
			?>
		  </select></td>
     <td width="136" bgcolor="#f9f9f9"><select name="departmentsold" >
			<?php
			$strSQL6 = "SELECT * FROM departments ORDER BY id ASC";
			$objQuery6 = mysql_query($strSQL6);
			while($objResuut6 = mysql_fetch_array($objQuery6))
			{
			?>
			<option value="<?php echo $objResuut6["id"];?>"><?php echo $objResuut6["abbr"];?></option>
			<?php
			}
			?>
		  </select></td>
     <td width="174" bgcolor="#f9f9f9"> <select name="users" >
			<?php
			$strSQL7 = "SELECT * FROM users ORDER BY id ASC";
			$objQuery7 = mysql_query($strSQL7);
			while($objResuut7 = mysql_fetch_array($objQuery7))
			{
			?>
			<option value="<?php echo $objResuut7["id"];?>"><?php echo $objResuut7["name"];?> <?php echo $objResuut7["lname"];?></option>
			<?php
			}
			?>
		  </select></td>
     <td width="299" bgcolor="#f9f9f9"> <select name="projects" >
			<?php
			$strSQL8 = "SELECT * FROM projects ORDER BY id ASC";
			$objQuery8 = mysql_query($strSQL8);
			while($objResuut8 = mysql_fetch_array($objQuery8))
			{
			?>
			<option value="<?php echo $objResuut8["id"];?>"><?php echo $objResuut8["name"];?></option>
			<?php
			}
			?>
		  </select></td>
          <td>
                  <select name="statuses">
			<?php
			$strSQL9 = "SELECT * FROM statuses ORDER BY id ASC";
			$objQuery9 = mysql_query($strSQL9);
			while($objResuut9 = mysql_fetch_array($objQuery9))
			{
			?>
			<option value="<?php echo $objResuut9["id"];?>"><?php echo $objResuut9["description"];?></option>
			<?php
			}
			?>
		  </select>
          </td>
          
          <td>
           <select name="code4">
			<?php
			$strSQL10 = "SELECT * FROM assets ORDER BY id ASC";
			$objQuery10 = mysql_query($strSQL10);
			while($objResuut10 = mysql_fetch_array($objQuery10))
			{
			?>
			<option value="<?php echo $objResuut10["id"];?>"><?php echo $objResuut10["code2"];?></option>
			<?php
			}
			?>
		  </select>
          </td>
      </tr>
      </table>
      <table>
      <tr>
     <td width="206" bgcolor="#dff0d8"><label for="asset_des">รายละเอียดครุภัณฑ์</label></td>
     <td width="194" bgcolor="#dff0d8"><label for="asset_income">รายละเอียดการได้มา</label></td>
     <td width="293" bgcolor="#dff0d8"><label for="asset_location">รายละเอียดที่ตั้ง</label></td>
     <td width="172" bgcolor="#dff0d8"> <label for="asset_relation">เอกสารที่เกี่ยวข้อง</label></td>
     
     </tr>
     <tr>
     <td width="206" bgcolor="#f9f9f9"> <textarea id="asset_des" name="asset_des"><?php echo $row["detail"];?></textarea></td>
     <td width="194" bgcolor="#f9f9f9"><textarea id="asset_income" name="asset_income" ><?php echo $row["detail_rcv"];?></textarea></td>
     <td width="293" bgcolor="#f9f9f9"><textarea id="asset_location" name="asset_location" ><?php echo $row["detail_location"];?></textarea></td>
     <td width="172" bgcolor="#f9f9f9"><textarea id="asset_relation" name="asset_relation" ><?php echo $row["doc_ref"];?></textarea></td>
     
      </tr>
      </table>
      <table>
      <tr>
      <td>
      <label for="asset_remark">หมายเหตุ</label>
      </td>
      </tr>
      <tr>
     <td>
      <textarea id="asset_remark" name="asset_remark" ><?php echo $row["remark"];?></textarea>
      </td>
      </tr>
      </table>   
      </div>  
  <input type="reset" value="เริ่มใหม่" width="10%">&nbsp;<input type="submit" name="submit" value="บันทึก" width="10%">
  </form>


</body>
</html>
 <?php

}



 if (isset($_POST['submit']))
 {

 $code = explode(".",mysql_real_escape_string(htmlspecialchars($_POST['code'])));
 $code2 = mysql_real_escape_string(htmlspecialchars($_POST['code']));
 $code1 = mysql_real_escape_string(htmlspecialchars($_POST['code1']));

 $gfmiscode = mysql_real_escape_string(htmlspecialchars($_POST['gfmiscode']));
 $years = substr(mysql_real_escape_string(htmlspecialchars($_POST['years'])),2);
 $years1 = mysql_real_escape_string(htmlspecialchars($_POST['years']));
 $unitprice = mysql_real_escape_string(htmlspecialchars($_POST['unitprice']));
 $lifeperiod = mysql_real_escape_string(htmlspecialchars($_POST['lifeperiod']));
 $budgettypes = mysql_real_escape_string(htmlspecialchars($_POST['budgettypes']));
  $rcvtypes = mysql_real_escape_string(htmlspecialchars($_POST['rcvtypes']));
 $brands = mysql_real_escape_string(htmlspecialchars($_POST['brands']));
 $model = mysql_real_escape_string(htmlspecialchars($_POST['model']));
 $serial_no = mysql_real_escape_string(htmlspecialchars($_POST['serial_no']));
 $unit_name = mysql_real_escape_string(htmlspecialchars($_POST['unit_name']));
 $file = mysql_real_escape_string(htmlspecialchars($_POST['photo']));
 $departments = mysql_real_escape_string(htmlspecialchars($_POST['departments']));
 
 $code11 =  $years.'-'.$code2.'-'.$code1;
 $code5 = $departments.$years.'/'.$code[0].'/'.$code2.'/'.$code1;
 //exit();
 $departmentsold = mysql_real_escape_string(htmlspecialchars($_POST['departmentsold']));
 $users = mysql_real_escape_string(htmlspecialchars($_POST['users']));
 $projects = mysql_real_escape_string(htmlspecialchars($_POST['projects']));
 $statuses = mysql_real_escape_string(htmlspecialchars($_POST['statuses']));
 $code4 = mysql_real_escape_string(htmlspecialchars($_POST['code4']));
 $asset_des = mysql_real_escape_string(htmlspecialchars($_POST['asset_des']));
 $asset_income = mysql_real_escape_string(htmlspecialchars($_POST['asset_income']));
 $asset_location = mysql_real_escape_string(htmlspecialchars($_POST['asset_location']));
 $asset_relation = mysql_real_escape_string(htmlspecialchars($_POST['asset_relation']));
 $asset_sup = mysql_real_escape_string(htmlspecialchars($_POST['asset_sup']));
 $asset_remark = mysql_real_escape_string(htmlspecialchars($_POST['asset_remark']));
 $asseid = mysql_real_escape_string(htmlspecialchars($_POST['asseid']));

 $result_d = mysql_query("SELECT * FROM departments WHERE abbr='$departments'")
 or die(mysql_error());
 $row_d = mysql_fetch_array($result_d);
 $desp = $row_d["id"];
 
$result_c = mysql_query("SELECT * FROM categories WHERE code ='$code'")
 or die(mysql_error());
 $row_c = mysql_fetch_array($result_c);
 $cate_id = $row_c["id"];
 
 $result_y = mysql_query("SELECT * FROM years WHERE year ='$years1'")
 or die(mysql_error());
 $row_y = mysql_fetch_array($result_y);
 $year_id = $row_y["id"];
 
 $start_date = mysql_real_escape_string(htmlspecialchars($_POST['rcvdate']));
 $start_date_new = (explode("/",$start_date));
 $start_date_new1 = ($start_date_new[2]-543)."-".$start_date_new[1]."-".$start_date_new[0];
 

 if ($code == '' )
 {

 $error = 'Please enter the details!';

 valid($code4,$code5,$code1,$asset_des,$model,$gfmiscode,$unitprice,$unit_name,$lifeperiod,$asset_income,$rcvtypes,$budgettypes,$desp,$asset_location,$statuses,$start_date_new1,$cate_id,$brands,$serial_no,$asset_remark,$departmentsold,$year_id,$asset_relation,$file,$asset_sup,$projects,$catedate,$catedate,$name,$error);
 }
 else
 {
 $catedate =date('Y-m-d H:i:s');
 mysql_query("UPDATE assets SET code1='$code11',code2='$code5',seq_no ='$code1',detail='$asset_des',model='$model',gfmiscode='$gfmiscode',unitprice='$unitprice',unit_name='$unit_name',lifeperiod='$lifeperiod',detail_rcv='$asset_income',rcvtype_id='$rcvtypes',budgettype_id='$budgettypes',location_dept_id='$desp',detail_location='$asset_location',status_id='$statuses',register_date='$start_date_new1',cat_id='$cate_id',brand_id='$brands',serial_no='$serial_no',useable='Y',remark='$asset_remark',owner_user_id='',owner_dept_id='$departmentsold',year_id='$year_id',doc_ref='$asset_relation',img='$file',supplier_name='$asset_sup',prj_id='$projects',created_at='$catedate' ,updated_at='$catedate' where id='$asseid'")
 or die(mysql_error());

 header("Location: main_assets_detail.php");

 }
 }
 else
 {
 valid('','','','');
 }
?>